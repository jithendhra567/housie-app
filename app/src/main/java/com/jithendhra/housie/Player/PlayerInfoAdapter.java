package com.jithendhra.housie.Player;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jithendhra.housie.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PlayerInfoAdapter  extends RecyclerView.Adapter<PlayerInfoAdapter.ViewHolder>{
    private List<PlayerInfo> playerInfos;
    private Context context;
    public PlayerInfoAdapter(Context context,List<PlayerInfo> playerInfos) {
        this.playerInfos = playerInfos;
        this.context = context;
    }
    public void update(List<PlayerInfo> playerInfos){
        this.playerInfos=playerInfos;
        notifyDataSetChanged();
    }
    @Override
    public PlayerInfoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.playerinfoitem, parent, false);
        PlayerInfoAdapter.ViewHolder viewHolder = new PlayerInfoAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PlayerInfoAdapter.ViewHolder holder, final int position) {
        GradientDrawable drawable = (GradientDrawable) holder.bg.getBackground();
        Picasso.get().load(playerInfos.get(position).getImage()).into(holder.icon);
        holder.title.setText(playerInfos.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return playerInfos.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView title;
        LinearLayout bg;
        ViewHolder(View itemView) {
            super(itemView);
            this.bg = itemView.findViewById(R.id.bg);
            this.icon =  itemView.findViewById(R.id.playericon);
            this.title = itemView.findViewById(R.id.playername);
        }
    }
}
