package com.jithendhra.housie.Player;

public class PlayerInfo {
    String image;
    String title;
    int numberoftickets;

    public void setNumberoftickets(int numberoftickets) {
        this.numberoftickets = numberoftickets;
    }

    public int getNumberoftickets() {
        return numberoftickets;
    }

    public PlayerInfo(String image, String title,int numberoftickets){
        this.image=image;
        this.numberoftickets=numberoftickets;
        this.title=title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
