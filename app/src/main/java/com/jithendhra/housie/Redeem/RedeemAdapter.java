package com.jithendhra.housie.Redeem;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jithendhra.housie.Player.PlayerInfo;
import com.jithendhra.housie.R;
import com.jithendhra.housie.Redeem.Redeem;
import com.jithendhra.housie.Redeem.RedeemItem;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RedeemAdapter  extends RecyclerView.Adapter<RedeemAdapter.ViewHolder>{
    private List<RedeemItem> redeemItems;
    private Context context;
    public RedeemAdapter(Context context,List<RedeemItem> redeemItems) {
        this.redeemItems = redeemItems;
        this.context = context;
    }
    @Override
    public RedeemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.redeem_item, parent, false);
        RedeemAdapter.ViewHolder viewHolder = new RedeemAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RedeemAdapter.ViewHolder holder, final int position) {
        holder.icon.setImageDrawable(ContextCompat.getDrawable(context,redeemItems.get(position).getImage()));
        holder.username_id.setText(redeemItems.get(position).getTransaction_id());
        holder.details.setText(redeemItems.get(position).getTransaction_details());
    }
    @Override
    public int getItemCount() {
        return redeemItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView details,username_id;
        ViewHolder(View itemView) {
            super(itemView);
            this.icon =  itemView.findViewById(R.id.transaction_type);
            this.details=itemView.findViewById(R.id.transaction_details);
            this.username_id=itemView.findViewById(R.id.transaction_username);
        }
    }
}
