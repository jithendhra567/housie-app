package com.jithendhra.housie.Redeem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.jithendhra.housie.Constants;
import com.jithendhra.housie.MainActivity;
import com.jithendhra.housie.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Redeem extends AppCompatActivity {
    TextView currentsupercoins;
    FirebaseFirestore db;
    TextInputEditText amount,upi_id;
    RecyclerView transactions;
    TabLayout transactiontype;
    CardView redeem;
    Dialog loading;
    boolean isPending = true;

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_reedem);

        final String TAG="ADS";
        final InterstitialAd interstitialAd = new InterstitialAd(this, "765047894257227_767002484061768");
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        });
        interstitialAd.loadAd();

        db=FirebaseFirestore.getInstance();
        loading=new Dialog(this);
        loading.setContentView(R.layout.loading);
        //load  current coins
        currentsupercoins=findViewById(R.id.currentsupercoins_redeem);
        currentsupercoins.setText(Constants.getUserInfo().getSUPER_COINS()+"");

        //redeem
        transactiontype = findViewById(R.id.transaction_type);
        transactions=findViewById(R.id.redeemitems);
        amount=findViewById(R.id.amount_redeem);
        redeem=findViewById(R.id.redeem);
        upi_id=findViewById(R.id.upi_id);


        db.collection(Constants.USERS).document(Constants.getUserInfo().getPHONE_NUMBER()).collection(Constants.TRANSPENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                System.out.println(task.getResult().isEmpty());
                if(!task.getResult().isEmpty()){
                    System.out.println("entering");
                    findViewById(R.id.notrans).setVisibility(View.INVISIBLE);
                    List<DocumentSnapshot> list = task.getResult().getDocuments();
                    List<RedeemItem> redeemItems = new ArrayList<>();
                    for(DocumentSnapshot d: list){
                        redeemItems.add(new RedeemItem(d.get(Constants.TRANSID).toString()+" | "+d.get(Constants.AMOUNT).toString()+" Rs"
                                ,d.get(Constants.USER_NAME)+" | "+d.get(Constants.UPIID),R.drawable.pending));
                    }
                    RedeemAdapter redeemAdapter = new RedeemAdapter(Redeem.this,redeemItems);
                    transactions.setLayoutManager(new LinearLayoutManager(Redeem.this));
                    transactions.setAdapter(redeemAdapter);
                }
                else findViewById(R.id.notrans).setVisibility(View.VISIBLE);
                loading.cancel();
            }
        });


        redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(amount.getText()+" "+upi_id.getText());
                if(!(amount.getText().toString().length()>0) && !(upi_id.getText().toString().length()>0)){
                    Toast.makeText(Redeem.this, "Please Enter your details to redeem", Toast.LENGTH_SHORT).show();
                    return;
                }
                final int supercoins = Integer.parseInt(amount.getText().toString());
                if((supercoins/10)*10!=supercoins && supercoins>=1000) {
                    Toast.makeText(Redeem.this, "Please Enter Valid Number like 1000,1200,3000.....", Toast.LENGTH_SHORT).show();
                    return;
                }
                loading();
                final String upiid = upi_id.getText().toString();
                final int[] c = {0};
                db.collection(Constants.USERS).document(Constants.getUserInfo().getPHONE_NUMBER()).collection(Constants.AMOUNT).document(Constants.AMOUNT).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful() && task.getResult()!=null){
                            DocumentSnapshot documentSnapshot = task.getResult();
                            try {
                                c[0]=Integer.parseInt(documentSnapshot.get(Constants.AMOUNT).toString())+1;
                            }
                            catch (Exception e){
                                c[0]=1;
                            }
                        }
                        else c[0]=1;
                        Map<String , Object> m = new HashMap<String, Object>(){{ put(Constants.AMOUNT,c[0]); }};
                        db.collection(Constants.USERS).document(Constants.getUserInfo().getPHONE_NUMBER()).collection(Constants.AMOUNT).document(Constants.AMOUNT).set(m);
                        String transactionid = "TC"+Constants.getUserInfo().getPHONE_NUMBER().substring(7)+""+c[0];
                        Map<String,Object> map = new HashMap<>();
                        map.put(Constants.AMOUNT,supercoins/10);
                        map.put(Constants.UPIID,upiid);
                        map.put(Constants.USER_NAME,Constants.getUserInfo().getUSER_NAME());
                        map.put(Constants.TRANSID,transactionid);
                        db.collection(Constants.REDEEM).document(transactionid).set(map);
                        db.collection(Constants.USERS).document(Constants.getUserInfo().getPHONE_NUMBER()).collection(Constants.TRANSPENDING).document(transactionid).set(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    db.collection(Constants.USERS).document(Constants.getUserInfo().getPHONE_NUMBER()).collection(Constants.TRANSPENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if(!task.getResult().isEmpty()){
                                                findViewById(R.id.notrans).setVisibility(View.INVISIBLE);
                                                List<DocumentSnapshot> list = task.getResult().getDocuments();
                                                List<RedeemItem> redeemItems = new ArrayList<>();
                                                for(DocumentSnapshot d: list){
                                                    redeemItems.add(new RedeemItem(d.get(Constants.TRANSID).toString()+" | "+d.get(Constants.AMOUNT).toString()+" Rs"
                                                            ,d.get(Constants.USER_NAME)+" | "+d.get(Constants.UPIID),R.drawable.pending));
                                                }
                                                RedeemAdapter redeemAdapter = new RedeemAdapter(Redeem.this,redeemItems);
                                                transactions.setLayoutManager(new LinearLayoutManager(Redeem.this));
                                                transactions.setAdapter(redeemAdapter);
                                            }
                                            else findViewById(R.id.notrans).setVisibility(View.VISIBLE);
                                            loading.cancel();
                                        }
                                    });
                                }
                            }
                        });
                    }
                });

            }
        });

        transactiontype.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                loading();
                if(tab.getText().toString().equals("Pending Transaction")) {
                    db.collection(Constants.USERS).document(Constants.getUserInfo().getPHONE_NUMBER()).collection(Constants.TRANSPENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if(!task.getResult().isEmpty()){
                                findViewById(R.id.notrans).setVisibility(View.INVISIBLE);
                                transactions.setVisibility(View.VISIBLE);
                                List<DocumentSnapshot> list = task.getResult().getDocuments();
                                if(list.size()==0) {
                                    findViewById(R.id.notrans).setVisibility(View.VISIBLE);
                                    transactions.setVisibility(View.INVISIBLE);
                                    loading.cancel();
                                    return;
                                }
                                List<RedeemItem> redeemItems = new ArrayList<>();
                                for(DocumentSnapshot d: list){
                                    redeemItems.add(new RedeemItem(d.get(Constants.TRANSID).toString()+" | "+d.get(Constants.AMOUNT).toString()+" Rs"
                                            ,d.get(Constants.USER_NAME)+" | "+d.get(Constants.UPIID),R.drawable.pending));
                                }
                                RedeemAdapter redeemAdapter = new RedeemAdapter(Redeem.this,redeemItems);
                                transactions.setLayoutManager(new LinearLayoutManager(Redeem.this));
                                transactions.setAdapter(redeemAdapter);
                            }
                            else{
                                findViewById(R.id.notrans).setVisibility(View.VISIBLE);
                                transactions.setVisibility(View.INVISIBLE);
                            }
                            loading.cancel();
                        }
                    });
                }
                else {
                    db.collection(Constants.USERS).document(Constants.getUserInfo().getPHONE_NUMBER()).collection(Constants.TRANSCOMPLETED).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if(!task.getResult().isEmpty()){
                                findViewById(R.id.notrans).setVisibility(View.INVISIBLE);
                                List<DocumentSnapshot> list = task.getResult().getDocuments();
                                if(list.size()==0) {
                                    findViewById(R.id.notrans).setVisibility(View.VISIBLE);
                                    transactions.setVisibility(View.INVISIBLE);
                                    loading.cancel();
                                    return;
                                }
                                List<RedeemItem> redeemItems = new ArrayList<>();
                                for(DocumentSnapshot d: list){
                                    redeemItems.add(new RedeemItem(d.get(Constants.TRANSID).toString()+" | "+d.get(Constants.AMOUNT).toString()+" Rs"
                                            ,d.get(Constants.USER_NAME)+" | "+d.get(Constants.UPIID),R.drawable.success));
                                }
                                RedeemAdapter redeemAdapter = new RedeemAdapter(Redeem.this,redeemItems);
                                transactions.setLayoutManager(new LinearLayoutManager(Redeem.this));
                                transactions.setAdapter(redeemAdapter);
                            }
                            else{
                                findViewById(R.id.notrans).setVisibility(View.VISIBLE);
                                transactions.setVisibility(View.INVISIBLE);
                            }
                            loading.cancel();
                        }
                    });
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private void loading() {
        loading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loading.show();
        loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false);
    }

}