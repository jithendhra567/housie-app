package com.jithendhra.housie.Redeem;

public class RedeemItem {
    String transaction_details,transaction_id;
    int image;

    public RedeemItem(String transaction_details, String transaction_id, int image) {
        this.transaction_details = transaction_details;
        this.transaction_id = transaction_id;
        this.image = image;
    }

    public String getTransaction_details() {
        return transaction_details;
    }

    public void setTransaction_details(String transaction_details) {
        this.transaction_details = transaction_details;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
