package com.jithendhra.housie.EndScreen;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jithendhra.housie.Player.PlayerInfo;
import com.jithendhra.housie.Player.PlayerInfoAdapter;
import com.jithendhra.housie.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EndItemAdapter  extends RecyclerView.Adapter<EndItemAdapter.ViewHolder>{
    private List<EndItem> endItems;
    private Context context;
    public EndItemAdapter(Context context,List<EndItem> endItems) {
        this.endItems = endItems;
        this.context = context;
    }
    public void update(List<EndItem> endItems){
        this.endItems=endItems;
        notifyDataSetChanged();
    }
    @Override
    public EndItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.enditem, parent, false);
        EndItemAdapter.ViewHolder viewHolder = new EndItemAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final EndItemAdapter.ViewHolder holder, final int position) {
        Picasso.get().load(endItems.get(position).getUsericon()).into(holder.icon);
        try {
            endItems.get(position).getWinitems().length();
            holder.profit.setText(endItems.get(position).getWinitems());
        }
        catch (Exception e){
            holder.profit.setText("EMPTY");
        }
        holder.username.setText(endItems.get(position).getUsername());
        holder.coins.setText("+"+endItems.get(position).getCoins()+" ");
    }

    @Override
    public int getItemCount() {
        return endItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView username,profit,coins;
        ViewHolder(View itemView) {
            super(itemView);
            this.coins=itemView.findViewById(R.id.coins);
            this.username=itemView.findViewById(R.id.win_playername);
            this.profit=itemView.findViewById(R.id.profit);
            this.icon=itemView.findViewById(R.id.end_playericon);
        }
    }
}
