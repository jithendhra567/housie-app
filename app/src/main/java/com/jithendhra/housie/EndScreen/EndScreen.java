package com.jithendhra.housie.EndScreen;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.SetOptions;
import com.jithendhra.housie.Constants;
import com.jithendhra.housie.GameScreen.GameScreen;
import com.jithendhra.housie.MainActivity;
import com.jithendhra.housie.Player.PlayerInfo;
import com.jithendhra.housie.Player.PlayerInfoAdapter;
import com.jithendhra.housie.R;
import com.jithendhra.housie.User.UserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class EndScreen extends AppCompatActivity {
    FirebaseFirestore db;
    RecyclerView  recyclerView;
    List<EndItem> endItems = new ArrayList<>();
    int typeofcoins,totalcoins=0;
    String roomid;
    private String TAG="ads";
    boolean isadshow=false;
    final Map<String,String> winners = new HashMap<>(),items= new HashMap<>(),prizepool = new HashMap<>();
    final ArrayList<String> usernames=new ArrayList<>(),usericons = new ArrayList<>();
    final ArrayList<Integer> numberoftickets = new ArrayList<>(),wincoins=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_end_screen);


        final InterstitialAd interstitialAd = new InterstitialAd(this, "765047894257227_766394890789194");
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        });
        interstitialAd.loadAd();


        recyclerView=findViewById(R.id.enditems);
        db=FirebaseFirestore.getInstance();
        usernames.addAll(Objects.requireNonNull(getIntent().getStringArrayListExtra(Constants.PLAYER)));
        usericons.addAll(getIntent().getStringArrayListExtra(Constants.AVATAR));
        numberoftickets.addAll(getIntent().getIntegerArrayListExtra(Constants.NUMBEROFTICKETS));
        typeofcoins=getIntent().getExtras().getInt(Constants.TYPEOFCOINS);
        winners.putAll(Constants.getWinners());
        items.putAll(Constants.getItems());
        prizepool.putAll(Constants.getPrizepool());
        for(int i=0;i<usernames.size();i++) {
            String[] a = winners.get(usernames.get(i)).split("\n");
            int total=0;
            if(a.length>0)
            for (String s:a) {
                float l = (items.containsKey(s)?Float.parseFloat(items.get(s)):1);
                total+=(int)Float.parseFloat(prizepool.get(s))/l;
            }
            Map<String,Object> map1 = new HashMap<>();
            if(typeofcoins==1){
                Constants.getUserInfo().setWIN_SUPER_COINS(Constants.getUserInfo().getWIN_SUPER_COINS()+total*50);
                Constants.getUserInfo().setSUPER_COINS(Constants.getUserInfo().getSUPER_COINS()+total);
                map1.put(Constants.SUPER_COINS,Constants.getUserInfo().getSUPER_COINS());
                map1.put(Constants.WIN_SUPER_COINS,Constants.getUserInfo().getWIN_SUPER_COINS());
                float ratio =  (Constants.getUserInfo().getWIN_SUPER_COINS()/(float)Constants.getUserInfo().getNUMBER_OF_MATCHES());
                Constants.getUserInfo().setLUCK_RATIO(ratio);
                map1.put(Constants.LUCK_RATIO,Constants.getUserInfo().getLUCK_RATIO());
            }
            else{
                Constants.getUserInfo().setWIN_COINS(Constants.getUserInfo().getWIN_SUPER_COINS()+total*50);
                Constants.getUserInfo().setCOINS(Constants.getUserInfo().getCOINS()+total);
                map1.put(Constants.COINS,Constants.getUserInfo().getCOINS());
                map1.put(Constants.WIN_COINS,Constants.getUserInfo().getWIN_SUPER_COINS());
                float ratio = (float) (Constants.getUserInfo().getWIN_COINS()/(float)Constants.getUserInfo().getNUMBER_OF_MATCHES());
                Constants.getUserInfo().setLUCK_RATIO(ratio);
                map1.put(Constants.LUCK_RATIO,Constants.getUserInfo().getLUCK_RATIO());
            }
            FirebaseFirestore.getInstance().collection(Constants.USERS).document(Constants.getUserInfo().getPHONE_NUMBER()).set(map1, SetOptions.merge())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(!task.isSuccessful())
                                Toast.makeText(EndScreen.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
            endItems.add(new EndItem(total, winners.get(usernames.get(i)), usernames.get(i), usericons.get(i)));
        }
        EndItemAdapter endItemAdapter = new EndItemAdapter(this,endItems);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(endItemAdapter);
    }

    public void done(View view) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
    }
}