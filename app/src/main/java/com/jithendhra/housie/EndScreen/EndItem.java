package com.jithendhra.housie.EndScreen;

public class EndItem {
    int coins;
    String username,usericon,winitems;

    public EndItem(int coins, String winitems, String username, String usericon) {
        this.coins = coins;
        this.winitems = winitems;
        this.username = username;
        this.usericon = usericon;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public void setUsericon(String usericon) {
        this.usericon = usericon;
    }

    public int getCoins() {
        return coins;
    }

    public String getWinitems() {
        return winitems;
    }

    public void setWinitems(String winitems) {
        this.winitems = winitems;
    }

    public String getUsername() {
        return username;
    }

    public String getUsericon() {
        return usericon;
    }
}
