package com.jithendhra.housie.User;

import com.jithendhra.housie.R;

public class UserInfo {
    String USER_NAME,PHONE_NUMBER,ROOM_ID,REFERRAL;
    float LUCK_RATIO;
    long SUPER_COINS,COINS,NUMBER_OF_MATCHES,WIN_COINS,WIN_SUPER_COINS,CONSUMED_COINS,CONSUMED_SUPER_COINS;
    String AVATAR;
    public UserInfo(){

    }

    public UserInfo(String USER_NAME, String PHONE_NUMBER, String ROOM_ID, float LUCK_RATIO, long COINS, long SUPER_COINS, long NUMBER_OF_MATCHES, long WIN_COINS, long WIN_SUPER_COINS, long CONSUMED_COINS, long CONSUMED_SUPER_COINS, String AVATAR,String REFERRAL) {
        this.USER_NAME = USER_NAME;
        this.PHONE_NUMBER = PHONE_NUMBER;
        this.ROOM_ID = ROOM_ID;
        this.LUCK_RATIO = LUCK_RATIO;
        this.SUPER_COINS = SUPER_COINS;
        this.COINS = COINS;
        this.NUMBER_OF_MATCHES = NUMBER_OF_MATCHES;
        this.WIN_COINS = WIN_COINS;
        this.WIN_SUPER_COINS = WIN_SUPER_COINS;
        this.CONSUMED_COINS = CONSUMED_COINS;
        this.CONSUMED_SUPER_COINS = CONSUMED_SUPER_COINS;
        this.AVATAR = AVATAR;
        this.REFERRAL = REFERRAL;
    }

    public String getUSER_NAME() {
        return USER_NAME;
    }

    public void setUSER_NAME(String USER_NAME) {
        this.USER_NAME = USER_NAME;
    }

    public String getREFERRAL() {
        return REFERRAL;
    }

    public void setREFERRAL(String REFERRAL) {
        this.REFERRAL = REFERRAL;
    }

    public String getPHONE_NUMBER() {
        return PHONE_NUMBER;
    }

    public void setPHONE_NUMBER(String PHONE_NUMBER) {
        this.PHONE_NUMBER = PHONE_NUMBER;
    }

    public String getROOM_ID() {
        return ROOM_ID;
    }

    public void setROOM_ID(String ROOM_ID) {
        this.ROOM_ID = ROOM_ID;
    }

    public long getSUPER_COINS() {
        return SUPER_COINS;
    }

    public void setSUPER_COINS(long SUPER_COINS) {
        this.SUPER_COINS = SUPER_COINS;
    }

    public long getCOINS() {
        return COINS;
    }

    public void setCOINS(long COINS) {
        this.COINS = COINS;
    }

    public long getNUMBER_OF_MATCHES() {
        return NUMBER_OF_MATCHES;
    }

    public void setNUMBER_OF_MATCHES(long NUMBER_OF_MATCHES) {
        this.NUMBER_OF_MATCHES = NUMBER_OF_MATCHES;
    }

    public long getWIN_COINS() {
        return WIN_COINS;
    }

    public void setWIN_COINS(long WIN_COINS) {
        this.WIN_COINS = WIN_COINS;
    }

    public long getWIN_SUPER_COINS() {
        return WIN_SUPER_COINS;
    }

    public void setWIN_SUPER_COINS(long WIN_SUPER_COINS) {
        this.WIN_SUPER_COINS = WIN_SUPER_COINS;
    }

    public float getLUCK_RATIO() {
        return LUCK_RATIO;
    }

    public void setLUCK_RATIO(float LUCK_RATIO) {
        this.LUCK_RATIO = LUCK_RATIO;
    }

    public long getCONSUMED_COINS() {
        return CONSUMED_COINS;
    }

    public void setCONSUMED_COINS(long CONSUMED_COINS) {
        this.CONSUMED_COINS = CONSUMED_COINS;
    }

    public long getCONSUMED_SUPER_COINS() {
        return CONSUMED_SUPER_COINS;
    }

    public void setCONSUMED_SUPER_COINS(long CONSUMED_SUPER_COINS) {
        this.CONSUMED_SUPER_COINS = CONSUMED_SUPER_COINS;
    }

    public String getAVATAR() {
        return AVATAR;
    }

    public void setAVATAR(String AVATAR) {
        this.AVATAR = AVATAR;
    }

}
