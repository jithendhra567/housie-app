package com.jithendhra.housie.User;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Transaction;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.jithendhra.housie.Constants;
import com.jithendhra.housie.MainActivity;
import com.jithendhra.housie.R;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class UserDetails extends AppCompatActivity implements View.OnClickListener {
    MaterialButton updateDetails;
    ImageView userimage;
    int usericon=0;
    UserInfo userInfo;
    EditText username;
    FirebaseFirestore db;
    FirebaseUser firebaseUser;
    CardView filemanager;
    TextInputEditText referral;
    String referral_code="";
    private int request_code=111;
    private Uri mImageUri;
    private String uploadimageuri="";
    private StorageReference mStorageRef;
    Dialog loading;
    boolean isreffered = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_details);
        db=FirebaseFirestore.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        updateDetails=findViewById(R.id.updatedetails);
        filemanager = findViewById(R.id.filemanager);
        referral=findViewById(R.id.referral);
        loading=new Dialog(this);
        loading.setContentView(R.layout.loading);
        userimage=findViewById(R.id.usericon);
        username=findViewById(R.id.username);
        userimage.setOnClickListener(this);
        filemanager.setOnClickListener(this);
        mStorageRef= FirebaseStorage.getInstance().getReference(Constants.AVATAR);
        updateDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String regularExpression = "^[[A-Z]|[a-z]][[A-Z]|[a-z]|\\d|[_]]{7,29}$";
                final String user = username.getText().toString();
                if(user.equals("") && !user.matches(regularExpression)) {
                    Toast.makeText(UserDetails.this, "Please Enter your name", Toast.LENGTH_SHORT).show();
                    return;
                }
                else if(uploadimageuri.equals("") | uploadimageuri.length()==0){
                    Toast.makeText(UserDetails.this, "Please Upload your Image By Clicking on profile Image", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(referral.getText().toString().length()==0) referral_code="#";
                else referral_code = referral.getText().toString();
                db.collection(Constants.REFERRAL).document(referral_code).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful() && task.getResult().exists()){
                            final DocumentSnapshot documentSnapshot = task.getResult();
                            int used = Integer.parseInt(documentSnapshot.get(Constants.USEDTIMES).toString());
                            if(used<3){
                                isreffered=true;
                                db.collection(Constants.USERS).document(documentSnapshot.get(Constants.PHONE_NUMBER).toString()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                       if(task.isSuccessful()){
                                           DocumentSnapshot documentSnapshot1 = task.getResult();
                                           Map<String,Object> map = new HashMap<>();
                                           map.put(Constants.SUPER_COINS,Integer.parseInt(documentSnapshot1.get(Constants.SUPER_COINS).toString())+50);
                                           db.collection(Constants.USERS).document(documentSnapshot.get(Constants.PHONE_NUMBER).toString()).update(map);
                                       }
                                    }
                                });
                                Map<String,Object> m  = new HashMap<>();
                                m.put(Constants.USEDTIMES,used+1);
                                db.collection(Constants.REFERRAL).document(referral_code).update(m);
                            }
                            else isreffered=false;
                        }
                        else isreffered=false;
                        db.collection(Constants.USER_NAMES).document(user).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if(task.isSuccessful()){
                                    DocumentSnapshot documentSnapshot = task.getResult();
                                    if(documentSnapshot!=null && documentSnapshot.exists()) {
                                        Toast.makeText(UserDetails.this, "Name already Taken by Some other user", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    else {
                                        Toast.makeText(UserDetails.this, "your data is updating please wait", Toast.LENGTH_SHORT).show();
                                        String REF = getAlphaNumericString(8);
                                        userInfo = new UserInfo(user,firebaseUser.getPhoneNumber(),"0",0,5000,50+((isreffered)?50:0),0,0,0,0,0,uploadimageuri,REF);
                                        final Map<String,Object> map = new HashMap<>();
                                        map.put(Constants.USER_NAME,userInfo.getUSER_NAME());
                                        map.put(Constants.PHONE_NUMBER,userInfo.getPHONE_NUMBER());
                                        map.put(Constants.ROOM_ID,userInfo.getROOM_ID());
                                        map.put(Constants.LUCK_RATIO,userInfo.getLUCK_RATIO());
                                        map.put(Constants.COINS,userInfo.getCOINS());
                                        map.put(Constants.SUPER_COINS,userInfo.getSUPER_COINS());
                                        map.put(Constants.NUMBER_OF_MATCHES,userInfo.getNUMBER_OF_MATCHES());
                                        map.put(Constants.WIN_COINS,userInfo.getWIN_COINS());
                                        map.put(Constants.WIN_SUPER_COINS,userInfo.getWIN_SUPER_COINS());
                                        map.put(Constants.CONSUMED_COINS,userInfo.getCONSUMED_COINS());
                                        map.put(Constants.CONSUMED_SUPER_COINS,userInfo.getCONSUMED_SUPER_COINS());
                                        map.put(Constants.REFERRAL,userInfo.getREFERRAL());
                                        map.put(Constants.AVATAR,userInfo.getAVATAR());
                                        db.collection(Constants.USERS).document(firebaseUser.getPhoneNumber()).set(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    Toast.makeText(UserDetails.this, "Successfully updated", Toast.LENGTH_SHORT).show();
                                                    Map<String,Object> usernames = new HashMap<>();
                                                    usernames.put(user,userInfo.getPHONE_NUMBER());
                                                    startActivity(new Intent(UserDetails.this, MainActivity.class).putExtra("code",101));
                                                    db.collection(Constants.USER_NAMES).document(user).set(usernames).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(!task.isSuccessful()) Toast.makeText(UserDetails.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                                    finish();
                                                }
                                                else
                                                    Toast.makeText(UserDetails.this, "Something went Wrong Please Check your Network and try Again", Toast.LENGTH_SHORT).show();

                                            }
                                        });
                                        Map<String,Object> m = new HashMap<>();
                                        m.put(Constants.PHONE_NUMBER,userInfo.getPHONE_NUMBER());
                                        m.put(Constants.USEDTIMES,0);
                                        db.collection(Constants.REFERRAL).document(REF).set(m);

                                    }
                                }
                            }
                        });
                    }
                });
            }
        });

    }


    static String getAlphaNumericString(int n)
    {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index = (int)(AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }
    private void loading() {
        loading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loading.show();
        loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == this.request_code && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            mImageUri = data.getData();
            loading();
            System.out.println(mImageUri.getQueryParameterNames());
            Uri returnUri = data.getData();
            Cursor returnCursor = getContentResolver().query(returnUri, null, null, null, null);
            Picasso.get().load(mImageUri).into(userimage);
            uploadFile();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.filemanager:
            case R.id.usericon:     Intent i=new Intent(Intent.ACTION_PICK);
                                    i.setType("image/*");
                                    startActivityForResult(i,request_code);
                                    break;
        }
    }

    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
    private void uploadFile() {
        if (mImageUri != null) {
            StorageReference fileReference = mStorageRef.child(System.currentTimeMillis()
                    + "." + getFileExtension(mImageUri));
            fileReference.putFile(mImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            taskSnapshot.getStorage().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    uploadimageuri=task.getResult().toString();
                                    loading.cancel();
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(UserDetails.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
        }

    }
}