package com.jithendhra.housie.Chat;

public class ChatItem {
    String image;
    String username,message;

    public ChatItem(String image, String username, String message) {
        this.image = image;
        this.username = username;
        this.message = message;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUsername() {
        return username;
    }

    public String getMessage() {
        return message;
    }

    public String getImage() {
        return image;
    }
}
