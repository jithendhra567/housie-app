package com.jithendhra.housie.Chat;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jithendhra.housie.Player.PlayerInfo;
import com.jithendhra.housie.Player.PlayerInfoAdapter;
import com.jithendhra.housie.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder>{
    private List<ChatItem> chatItems;
    private Context context;
    public ChatAdapter(Context context,List<ChatItem> chatItems) {
        this.chatItems = chatItems;
        this.context = context;
    }
    public void update(List<ChatItem> chatItems){
        this.chatItems=chatItems;
        notifyDataSetChanged();
    }
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.chatitem, parent, false);
        ChatAdapter.ViewHolder viewHolder = new ChatAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ChatAdapter.ViewHolder holder, final int position) {
        Picasso.get().load(chatItems.get(position).getImage()).into(holder.icon);
        holder.message.setText(chatItems.get(position).getMessage());
        holder.username.setText(chatItems.get(position).getUsername());
    }

    @Override
    public int getItemCount() {
        return chatItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView username,message;
        ViewHolder(View itemView) {
            super(itemView);
            this.username = itemView.findViewById(R.id.chatusername);
            this.icon =  itemView.findViewById(R.id.chaticon);
            this.message = itemView.findViewById(R.id.chatmessage);
        }
    }
}