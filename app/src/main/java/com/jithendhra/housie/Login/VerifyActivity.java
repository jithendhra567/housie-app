package com.jithendhra.housie.Login;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.jithendhra.housie.Constants;
import com.jithendhra.housie.MainActivity;
import com.jithendhra.housie.R;

import java.util.concurrent.TimeUnit;


public class VerifyActivity extends AppCompatActivity implements View.OnKeyListener {
    EditText digit1,digit2,digit3,digit4,digit5,digit6;
    CardView verify;
    String PhoneNumber;
    FirebaseAuth auth;
    String verificationid;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    Dialog loading;
    int index=0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_verify);
        loading=new Dialog(this);
        loading.setContentView(R.layout.loading);
        //Digits
        digit1=findViewById(R.id.digit1);
        digit2=findViewById(R.id.digit2);
        digit3=findViewById(R.id.digit3);
        digit4=findViewById(R.id.digit4);
        digit5=findViewById(R.id.digit5);
        digit6=findViewById(R.id.digit6);
        verify=findViewById(R.id.verify);
        digit1.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        ImageView imageView = findViewById(R.id.image);
        ViewGroup.LayoutParams l = imageView.getLayoutParams();
        l.height= (int) (getResources().getDisplayMetrics().heightPixels*0.3);
        imageView.setLayoutParams(l);
        listners();

        if (getIntent().getExtras() != null) {
            if (getIntent().hasExtra(Constants.PHONE_NUMBER)) PhoneNumber = getIntent().getStringExtra(Constants.PHONE_NUMBER);
        }
        TextView edit = findViewById(R.id.editphonenumber);
        edit.setText(PhoneNumber);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        auth=FirebaseAuth.getInstance();
        callbacks= new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                loading();
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                Toast.makeText(VerifyActivity.this, "failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println(e.getCause()+"\n"+e.getMessage());
                if (e instanceof FirebaseAuthInvalidCredentialsException)
                    Toast.makeText(VerifyActivity.this, "Wrong Credentials", Toast.LENGTH_SHORT).show();
                else if (e instanceof FirebaseTooManyRequestsException)
                    Toast.makeText(VerifyActivity.this, "Too Many Request Please Try Later", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                Toast.makeText(VerifyActivity.this, "Code Received!!", Toast.LENGTH_SHORT).show();
                verificationid = s;
            }
        };

        startPhoneNumberVerification("+91"+PhoneNumber);
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading();
                if(digit1.getText().toString().length()==0 ||
                        digit2.getText().toString().length()==0 ||
                        digit3.getText().toString().length()==0 ||
                        digit4.getText().toString().length()==0 ||
                        digit5.getText().toString().length()==0 ||
                        digit6.getText().toString().length()==0){
                    Toast.makeText(VerifyActivity.this, "Please Enter otp correctly", Toast.LENGTH_SHORT).show();
                    return;
                }
                verifyPhoneNumberWithCode(verificationid,
                        digit1.getText().toString().trim() +
                                digit2.getText().toString().trim() +
                                digit3.getText().toString().trim() +
                                digit4.getText().toString().trim() +
                                digit5.getText().toString().trim() +
                                digit6.getText().toString().trim());
            }
        });


    }

    private void listners() {
        digit1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(digit1.getText().toString().length()>0) {
                    digit2.requestFocus();
                    digit2.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        digit2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(digit2.getText().toString().length()>0) {
                    digit3.requestFocus();
                    digit3.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        digit3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(digit3.getText().toString().length()>0) {
                    digit4.requestFocus();
                    digit4.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        digit4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(digit4.getText().toString().length()>0) {
                    digit5.requestFocus();
                    digit5.setCursorVisible(true);
                }

            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        digit5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(digit5.getText().toString().length()>0) {
                    digit6.requestFocus();
                    digit6.setCursorVisible(true);
                }

            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        digit6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(digit6.getText().toString().length()>0) {
                    digit6.requestFocus();
                    digit6.setCursorVisible(true);
                }

            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        digit1.setOnKeyListener(this);
        digit2.setOnKeyListener(this);
        digit3.setOnKeyListener(this);
        digit4.setOnKeyListener(this);
        digit5.setOnKeyListener(this);
        digit6.setOnKeyListener(this);
    }


    private void loading() {
        loading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loading.show();
        loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false);
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }
    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                30,
                TimeUnit.SECONDS,
                this,
                callbacks);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(VerifyActivity.this, "Your Phone Number is verified", Toast.LENGTH_SHORT).show();
                            loading.cancel();
                            startActivity(new Intent(VerifyActivity.this, MainActivity.class).putExtra("code",100));
                            finish();
                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException)
                                Toast.makeText(VerifyActivity.this, "Invalid code", Toast.LENGTH_SHORT).show();
                            else
                                Toast.makeText(VerifyActivity.this, "wrong code", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public boolean onKey(final View v, final int keyCode, KeyEvent event) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (v.getId()){
                            case R.id.digit2:
                                if(keyCode==67) {
                                    System.out.println("2");
                                    digit1.requestFocus();
                                    digit1.setCursorVisible(true);
                                }
                                break;
                            case R.id.digit3:
                                if(keyCode==67) {
                                    System.out.println("3");
                                    digit2.requestFocus();
                                    digit2.setCursorVisible(true);
                                }
                                break;
                            case R.id.digit4:
                                if(keyCode==67) {
                                    System.out.println("4");
                                    digit3.requestFocus();
                                    digit3.setCursorVisible(true);
                                }
                                break;
                            case R.id.digit5:
                                if(keyCode==67) {
                                    System.out.println("5");
                                    digit4.requestFocus();
                                    digit4.setCursorVisible(true);
                                }
                                break;
                            case R.id.digit6:
                                if(keyCode==67) {
                                    System.out.println("6");
                                    digit5.requestFocus();
                                    digit5.setCursorVisible(true);
                                }
                                break;
                        }
                    }
                });
            }
        },100);
        return false;
    }
}
