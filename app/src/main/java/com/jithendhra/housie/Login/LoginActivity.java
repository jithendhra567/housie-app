package com.jithendhra.housie.Login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.jithendhra.housie.Constants;
import com.jithendhra.housie.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        ImageView imageView = findViewById(R.id.cardView);
        ViewGroup.LayoutParams l = imageView.getLayoutParams();
        l.height= (int) (getResources().getDisplayMetrics().heightPixels*0.3);
        imageView.setLayoutParams(l);
        CardView button = findViewById(R.id.getopt);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText textInputEditText = findViewById(R.id.phonenumber);
                String phonenumber = textInputEditText.getText().toString();
                if(phonenumber.length()==10 && valid(phonenumber))
                    startActivity(new Intent(LoginActivity.this,VerifyActivity.class).putExtra(Constants.PHONE_NUMBER,phonenumber));
                else Toast.makeText(LoginActivity.this, "please Enter a Valid Phone Number", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean valid(String s) {
        for (int i = 0; i < s.length(); i++) {
            try {
                Integer.parseInt(s.charAt(i)+"");
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }
}