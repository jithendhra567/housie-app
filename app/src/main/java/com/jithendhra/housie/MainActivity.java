package com.jithendhra.housie;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bluehomestudio.luckywheel.LuckyWheel;
import com.bluehomestudio.luckywheel.OnLuckyWheelReachTheTarget;
import com.bluehomestudio.luckywheel.WheelItem;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.jithendhra.housie.GameScreen.GameScreen;
import com.jithendhra.housie.Loading.LoadingScreen;
import com.jithendhra.housie.Login.LoginActivity;
import com.jithendhra.housie.Login.VerifyActivity;
import com.jithendhra.housie.Payments.SuperCoinsPayments;
import com.jithendhra.housie.Profile.Profile;
import com.jithendhra.housie.Redeem.Redeem;
import com.jithendhra.housie.User.UserDetails;
import com.jithendhra.housie.User.UserInfo;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.facebook.ads.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import in.goodiebag.carouselpicker.CarouselPicker;
import me.samlss.bling.Bling;
import me.samlss.bling.BlingType;


public class MainActivity extends AppCompatActivity {
    Dialog loading;
    FirebaseFirestore db;
    FirebaseUser user;
    UserInfo userInfo;
    int code=0;
    AlertDialog alert;
    List<DocumentSnapshot> updates=new ArrayList<>();
    int currentdocumentlist=0;
    ConstraintLayout matching;
    LinearLayout addsupercoins;
    int numberoftickets=1,typeofcoins=0;
    TabLayout nof,toc;
    TextView matchingtext,supercoins,coins,onlineplayers,username;
    ImageView cancelmatching;
    boolean isMacthing=false,isLeaderOnline=false,roomcreated=false;
    private int docslen;
    ImageView profile;
    //ads
    private InterstitialAd coinad,ticketad;
    private String TAG="ads";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        checkNetworkConnectivity();
        //facebook ads
        AudienceNetworkAds
                .buildInitSettings(MainActivity.this)
                .withInitListener(new AudienceNetworkAds.InitListener() {
                    @Override
                    public void onInitialized(AudienceNetworkAds.InitResult initResult) {
                        System.out.println(initResult.getMessage());
                    }
                })
                .initialize();

        final InterstitialAd interstitialAd = new InterstitialAd(this, "765047894257227_AAA767092880719395");
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                Log.e(TAG, "Interstitial ad displayed.");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
                Log.e(TAG, "Interstitial ad dismissed.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
                interstitialAd.show();
            }

            @Override
            public void onAdClicked(Ad ad) {
                Toast.makeText(MainActivity.this, "Ad Clicked", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Interstitial ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
                Log.d(TAG, "Interstitial ad impression logged!");
            }
        });
        interstitialAd.loadAd();
        matching = findViewById(R.id.cl);
        onlineplayers=findViewById(R.id.onlineplayers);
        profile=findViewById(R.id.profileicon);
        username=findViewById(R.id.pro_username);
        matchingtext=findViewById(R.id.matchingtext);
        supercoins=findViewById(R.id.supercoins);
        coins=findViewById(R.id.coins);
        addsupercoins=findViewById(R.id.addsupercoins);
        cancelmatching = findViewById(R.id.cancel_matching);
        loading=new Dialog(this);
        loading.setContentView(R.layout.loading);
        loading();
        userInfo = new UserInfo();
        try {
            code = getIntent().getExtras().getInt("code");
        }
        catch (Exception e){
            code = 0;
        }
        try{
            user= FirebaseAuth.getInstance().getCurrentUser();
        }
        catch (Exception e){
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
        }

        db=FirebaseFirestore.getInstance();
        //updates
        db.collection(Constants.UPDATES).document(Constants.MAINTAINENCE).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if(documentSnapshot.exists() && documentSnapshot.get(Constants.MAINTAINENCE).toString().equals("1")){
                    startActivity(new Intent(MainActivity.this,Maintainence.class));
                    finish();
                }
            }
        });
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionName = packageInfo.versionName;
            final int versionCode = packageInfo.versionCode;
            db.collection(Constants.UPDATES).document(Constants.VERSION).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){
                        if(Integer.parseInt(task.getResult().get(Constants.VERSION).toString())!=versionCode){
                            Toast.makeText(MainActivity.this, "Please Install Latest version", Toast.LENGTH_SHORT).show();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    MainActivity.super.onBackPressed();
                                }
                            },1000);
                        }
                    }
                }
            });
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        try {
            db.collection(Constants.USERS).document(Objects.requireNonNull(user.getPhoneNumber()))
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot documentSnapshot = task.getResult();
                                if (documentSnapshot.exists() && code == 100) {
                                    fillDetails(documentSnapshot);
                                    Toast.makeText(MainActivity.this, "Welcome Back! " + userInfo.getUSER_NAME(), Toast.LENGTH_SHORT).show();
                                } else if (!documentSnapshot.exists()) {
                                    startActivity(new Intent(MainActivity.this, UserDetails.class));
                                    finish();
                                } else fillDetails(documentSnapshot);
                                coins.setText(userInfo.getCOINS() + "");
                                supercoins.setText(userInfo.getSUPER_COINS() + "");
                            }
                        }
                    });
        }
        catch (Exception e){
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
            finish();
        }
        db.collection(Constants.ONLINE).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                onlineplayers.setText("  "+snapshots.getDocuments().size()+" Online Players");
            }
        });
        final Timer[] timer = new Timer[1];
        final int[] i = {0};
        final TextView time = findViewById(R.id.timer);
        matching.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                vibrate();
                if(typeofcoins==-1) {
                    Toast.makeText(MainActivity.this, "Please Select Type of Coins", Toast.LENGTH_SHORT).show();
                    return;
                }
                else if(numberoftickets==0){
                    Toast.makeText(MainActivity.this, "please Select number of tickets", Toast.LENGTH_SHORT).show();
                    return;
                }
                AnimationDrawable drawable = (AnimationDrawable)matching.getBackground();
                drawable.setEnterFadeDuration(1000);
                drawable.setExitFadeDuration(1000);
                if(!isMacthing) {
                    drawable.start();
                    cancelmatching.setVisibility(View.VISIBLE);
                    timer[0] = new Timer();
                    time.setVisibility(View.VISIBLE);
                    matchingtext.setText("Matching ");
                    int size = (int) (getResources().getDisplayMetrics().widthPixels*0.018);
                    matchingtext.setAutoSizeTextTypeUniformWithConfiguration(10,size,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,TypedValue.COMPLEX_UNIT_SP);
                    isMacthing=true;
                    final int a = new Random().nextInt(10);
                    timer[0].scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(i[0]>=15){
                                        if(a+15==i[0]){
                                            String s = userInfo.getPHONE_NUMBER().substring(userInfo.getPHONE_NUMBER().length()-4);
                                            int rand =  1000 + (int)(Math.random() * 10000);
                                            final String room_id=s.concat(String.valueOf(rand));
                                            Map<String,Object> m = new HashMap<>();
                                            m.put(Constants.ROOM_ID,room_id);
                                            db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).update(m);
                                        }
                                    }
                                    if(i[0]>=10) time.setText(""+i[0]++);
                                    else time.setText("0"+i[0]++);
                                }
                            });
                        }
                    },0,1000);
                    db.collection((typeofcoins==0)?Constants.MATCHING_0:Constants.Matching_1).addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                            if(!value.isEmpty() && !roomcreated){
                                docslen = value.getDocuments().size();
                                joinMatchingRoom(value.getDocuments());
                            }
                        }
                    });
                }
                else{
                    db.collection((typeofcoins==0)?Constants.MATCH_0:Constants.MATCH_1).document(userInfo.getPHONE_NUMBER()).delete();
                    time.setVisibility(View.INVISIBLE);
                    timer[0].cancel();
                    i[0]=0;
                    drawable.stop();
                    cancelmatching.setVisibility(View.INVISIBLE);
                    matchingtext.setText("play");
                    matchingtext.setAutoSizeTextTypeUniformWithConfiguration(10,30,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,TypedValue.COMPLEX_UNIT_SP);
                    isMacthing=false;
                }
            }
        });


        //setting match
        CardView m = findViewById(R.id.start_matching);
        ViewGroup.LayoutParams layoutParams = m.getLayoutParams();
        layoutParams.width= (int) (getResources().getDisplayMetrics().widthPixels*0.35);
        m.setLayoutParams(layoutParams);
        //setting carousel
        try {
            updates.addAll(Constants.getUpdates());
        }
        catch (Exception e){
            startActivity(new Intent(this,SplashScreen.class));
            finish();
        }
        final CarouselView carouselView = findViewById(R.id.whatsnew);
        carouselView.setPageCount(updates.size());
        carouselView.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                Picasso.get().load(updates.get(position).get(Constants.POSTER).toString()).into(imageView);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        vibrate();
                        if(updates.get(carouselView.getCurrentItem()).get(Constants.TYPE).toString().equals(Constants.TUTORIAL))
                        watchYoutubeVideo(updates.get(carouselView.getCurrentItem()).get(Constants.ACTION).toString());
                        else if(updates.get(carouselView.getCurrentItem()).get(Constants.TYPE).toString().equals(Constants.ANNOUNCEMENTS)){
                            final Dialog dialog = new Dialog(MainActivity.this);
                            dialog.setContentView(R.layout.announcements);
                            int width = (int) (getResources().getDisplayMetrics().widthPixels*0.9);
                            int height = (int) (getResources().getDisplayMetrics().heightPixels*0.75);
                            dialog.getWindow().setLayout(width,height);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            TextView title=dialog.findViewById(R.id.title_dialog),des=dialog.findViewById(R.id.description),actiontext=dialog.findViewById(R.id.action_text);
                            MaterialCardView action = dialog.findViewById(R.id.action);
                            title.setText(updates.get(carouselView.getCurrentItem()).get(Constants.TITLE).toString());
                            des.setText(updates.get(carouselView.getCurrentItem()).get(Constants.DESCRIPTION).toString());
                            actiontext.setText("Ok");
                            action.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    vibrate();
                                    dialog.cancel();
                                }
                            });
                            dialog.show();
                        }
                        else if(updates.get(carouselView.getCurrentItem()).get(Constants.TYPE).toString().equals(Constants.TOURNAMENTS)){
                            final DocumentSnapshot documentSnapshot = updates.get(carouselView.getCurrentItem());
                            final Dialog dialog = new Dialog(MainActivity.this);
                            dialog.setContentView(R.layout.announcements);
                            int width = (int) (getResources().getDisplayMetrics().widthPixels*0.9);
                            int height = (int) (getResources().getDisplayMetrics().heightPixels*0.75);
                            dialog.getWindow().setLayout(width,height);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            TextView title=dialog.findViewById(R.id.title_dialog),des=dialog.findViewById(R.id.description),actiontext=dialog.findViewById(R.id.action_text);
                            MaterialCardView action = dialog.findViewById(R.id.action);
                            title.setText(updates.get(carouselView.getCurrentItem()).get(Constants.TITLE).toString());
                            des.setText(updates.get(carouselView.getCurrentItem()).get(Constants.DESCRIPTION).toString());
                            actiontext.setText(updates.get(carouselView.getCurrentItem()).get(Constants.ACTIONTEXT).toString());
                            action.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    System.out.println(documentSnapshot.getData());
                                    watchYoutubeVideo(documentSnapshot.get(Constants.ACTION).toString());
                                }
                            });
                            dialog.show();
                        }
                    }
                });
            }
        });
        carouselView.setKeepScreenOn(true);
        addsupercoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate();
                startActivity(new Intent(MainActivity.this, SuperCoinsPayments.class));
                finish();
            }
        });

    }


    public void vibrate(){
        final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) vibrator.vibrate(VibrationEffect.createOneShot(20, VibrationEffect.DEFAULT_AMPLITUDE));
        else vibrator.vibrate(20);
    }


    public void watchYoutubeVideo(String id) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(id));
        try {
            startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            startActivity(webIntent);
        }
    }

    private void checkNetworkConnectivity() {
        ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                Toast.makeText(MainActivity.this, "connected", Toast.LENGTH_SHORT).show();
                if(alert!=null && alert.isShowing()) alert.cancel();
            }

            @Override

            public void onLost(Network network) {
               AlertDialog.Builder builder= new AlertDialog.Builder(MainActivity.this);
               builder.setMessage("you have been disconnected from your network! \nplease turn on your mobile data or wifi")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(getApplicationContext(),"Game Over", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });
                alert = builder.create();
                alert.setTitle("Alert");
                alert.show();
            }
        };

        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) connectivityManager.registerDefaultNetworkCallback(networkCallback);
        else {
            NetworkRequest request = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET).build();
            connectivityManager.registerNetworkCallback(request, networkCallback);
        }
    }

    private void joinMatchingRoom(final List<DocumentSnapshot> List) {
        List<DocumentSnapshot> documentSnapshots = List;
        System.out.println(currentdocumentlist+" | "+docslen);
        if(currentdocumentlist>=docslen) {
            Map<String,Object> map = new HashMap<>();
            map.put(Constants.USER_NAME,userInfo.getUSER_NAME());
            db.collection((typeofcoins==0)?Constants.MATCH_0:Constants.MATCH_1).document(userInfo.getPHONE_NUMBER()).set(map);
        }
        else if(Integer.parseInt(documentSnapshots.get(currentdocumentlist).get(Constants.PLAYER).toString())<=10 && !documentSnapshots.get(currentdocumentlist).get(Constants.START).toString().equals("2")){
            final String room_id=documentSnapshots.get(currentdocumentlist).getId();
            long date = new Date(Long.parseLong(documentSnapshots.get(currentdocumentlist).get(Constants.DATA).toString())).getTime();
            long diff = new Date().getTime()-date;
            long diffMinutes = diff / (60 * 1000) % 60;
            if(diffMinutes>=1) {
                db.collection((typeofcoins==0)?Constants.MATCHING_0:Constants.Matching_1).document(room_id).delete();
                currentdocumentlist++;
                joinMatchingRoom(documentSnapshots);
            }
            else{
                db.collection(room_id).document((typeofcoins==0)?Constants.USER_0:Constants.USER_1).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful() && task.getResult()!=null){
                            roomcreated=true;
                            final DocumentSnapshot d =task.getResult();
                            Map<String,Object> ma = new HashMap<>();
                            ma.put(userInfo.getUSER_NAME(),new Date().getTime());
                            db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).collection(Constants.ROOMIDS).document(room_id).set(ma).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                }
                            });
                            Map<String,Object> m = new HashMap<>();
                            List<Integer> numbers=new ArrayList<>(),randomValues = new ArrayList<>();
                            if(d.exists()) m.put(userInfo.getPHONE_NUMBER(),numberoftickets);
                            else {
                                for(int i=1;i<=90;i++) numbers.add(i);
                                for(int i=0;i<90;i++){
                                    int ind = new Random().nextInt(numbers.size());
                                    randomValues.add(numbers.get(ind));
                                    numbers.remove(ind);
                                }
                                m.put(userInfo.getPHONE_NUMBER(),numberoftickets);
                                m.put(Constants.RANDOMNUMBER,randomValues);
                            }
                            db.collection(room_id).document((typeofcoins==0)?Constants.USER_0:Constants.USER_1).set(m,SetOptions.merge()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()) {
                                        Map<String,Object> map1 = new HashMap<>();
                                        if(typeofcoins==1){
                                            userInfo.setCONSUMED_SUPER_COINS(userInfo.getCONSUMED_SUPER_COINS()+numberoftickets*50);
                                            userInfo.setSUPER_COINS(userInfo.getSUPER_COINS()-numberoftickets*50);
                                            map1.put(Constants.SUPER_COINS,userInfo.getSUPER_COINS());
                                            map1.put(Constants.CONSUMED_SUPER_COINS,userInfo.getSUPER_COINS());
                                        }
                                        else{
                                            userInfo.setCONSUMED_COINS(userInfo.getCONSUMED_COINS()+numberoftickets*50);
                                            userInfo.setCOINS(userInfo.getCOINS()-numberoftickets*50);
                                            map1.put(Constants.COINS,userInfo.getCOINS());
                                            map1.put(Constants.COINS,userInfo.getCONSUMED_COINS());
                                        }
                                        db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).set(map1,SetOptions.merge())
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if(!task.isSuccessful())
                                                            Toast.makeText(MainActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                        startActivity(new Intent(MainActivity.this, LoadingScreen.class)
                                                .putExtra(Constants.ROOM_ID, room_id)
                                                .putExtra(Constants.LEADER,false)
                                                .putExtra(Constants.NUMBEROFTICKETS,numberoftickets)
                                                .putExtra(Constants.TYPEOFCOINS,typeofcoins)
                                                .putExtra(Constants.CUSTOMROOM,false));
                                        finish();
                                        db.collection(room_id).document((typeofcoins==0)?Constants.USER_0:Constants.USER_1).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if(task.isSuccessful() && task.getResult()!=null){
                                                    Map<String,Object> data = task.getResult().getData();
                                                    Map<String,Object> roomdata= new HashMap<>();
                                                    roomdata.put(Constants.PLAYER,data.size());
                                                    db.collection((typeofcoins==0)?Constants.MATCHING_0:Constants.Matching_1).document(room_id).set(roomdata,SetOptions.merge());
                                                }
                                            }
                                        });

                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
        else {
            final String room_id=documentSnapshots.get(currentdocumentlist).getId();
            db.collection((typeofcoins==0)?Constants.MATCHING_0:Constants.Matching_1).document(room_id).delete();
            currentdocumentlist++;
            joinMatchingRoom(documentSnapshots);
        }
    }

    private void loading() {
        loading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loading.show();
        loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false);
    }

    private void fillDetails(final DocumentSnapshot documentSnapshot) {
        userInfo.setUSER_NAME(documentSnapshot.get(Constants.USER_NAME).toString());
        userInfo.setPHONE_NUMBER(documentSnapshot.get(Constants.PHONE_NUMBER).toString());
        //System.out.println(documentSnapshot.get(Constants.COINS).toString());
        userInfo.setCOINS(Long.parseLong(documentSnapshot.get(Constants.COINS).toString()));
        userInfo.setSUPER_COINS(Long.parseLong(documentSnapshot.get(Constants.SUPER_COINS).toString()));
        userInfo.setROOM_ID("0");
        userInfo.setNUMBER_OF_MATCHES(Long.parseLong(documentSnapshot.get(Constants.NUMBER_OF_MATCHES).toString()));
        userInfo.setWIN_COINS(Long.parseLong(documentSnapshot.get(Constants.WIN_COINS).toString()));
        userInfo.setWIN_SUPER_COINS(Long.parseLong(documentSnapshot.get(Constants.WIN_SUPER_COINS).toString()));
        userInfo.setCONSUMED_COINS(Long.parseLong(documentSnapshot.get(Constants.CONSUMED_COINS).toString()));
        userInfo.setCONSUMED_SUPER_COINS(Long.parseLong(documentSnapshot.get(Constants.CONSUMED_SUPER_COINS).toString()));
        userInfo.setLUCK_RATIO(Float.parseFloat(documentSnapshot.get(Constants.LUCK_RATIO).toString()));
        userInfo.setAVATAR(documentSnapshot.get(Constants.AVATAR).toString());
        userInfo.setREFERRAL(documentSnapshot.get(Constants.REFERRAL).toString());
        Constants.setUserInfo(userInfo);

        username.setText(userInfo.getUSER_NAME());
        Picasso.get().load(userInfo.getAVATAR()).into(profile);
        Map<String,Object> map = new HashMap<>();
        map.put(userInfo.getPHONE_NUMBER(),Constants.ONLINE);
        db.collection(Constants.ONLINE).document(userInfo.getUSER_NAME()).set(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                //making user online
                if(task.isSuccessful()){
                    loading.cancel();
                }
            }
        });

        //matching
        db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                System.out.println(isMacthing +"  | "+ documentSnapshot.exists()+" | "+roomcreated);
                if(isMacthing && documentSnapshot.exists()&&!roomcreated){
                    roomcreated=true;
                    final String roomid = documentSnapshot.get(Constants.ROOM_ID).toString();
                    db.collection(roomid).document((typeofcoins==0)?Constants.USER_0:Constants.USER_1).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if(task.isSuccessful() && task.getResult()!=null){
                                final DocumentSnapshot d =task.getResult();
                                Map<String,Object> ma = new HashMap<>();
                                ma.put(userInfo.getUSER_NAME(),new Date().getTime());
                                db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).collection(Constants.ROOMIDS).document(roomid).set(ma).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                    }
                                });
                                Map<String,Object> m = new HashMap<>(),ran= new HashMap<>();
                                List<Integer> numbers=new ArrayList<>(),randomValues = new ArrayList<>();
                                m.put(userInfo.getPHONE_NUMBER(),numberoftickets);
                                if(!d.exists()){
                                    for(int i=1;i<=90;i++) numbers.add(i);
                                    for(int i=0;i<90;i++){
                                        int ind = new Random().nextInt(numbers.size());
                                        randomValues.add(numbers.get(ind));
                                        numbers.remove(ind);
                                    }
                                    ran.put(Constants.RANDOMNUMBER,randomValues);
                                    Map<String,Object> start = new HashMap<>();
                                    start.put(Constants.START,"0");
                                    db.collection(roomid).document(Constants.START).set(start);
                                    db.collection(roomid).document(Constants.RANDOMNUMBERLIST).set(ran);
                                }
                                db.collection(roomid).document((typeofcoins==0)?Constants.USER_0:Constants.USER_1).set(m,SetOptions.merge()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()) {
                                            Map<String,Object> map1 = new HashMap<>();
                                            if(typeofcoins==1){
                                                userInfo.setCONSUMED_SUPER_COINS(userInfo.getCONSUMED_SUPER_COINS()+numberoftickets*50);
                                                userInfo.setSUPER_COINS(userInfo.getSUPER_COINS()-numberoftickets*50);
                                                map1.put(Constants.SUPER_COINS,userInfo.getSUPER_COINS());
                                                map1.put(Constants.CONSUMED_SUPER_COINS,userInfo.getSUPER_COINS());
                                            }
                                            else{
                                                userInfo.setCONSUMED_COINS(userInfo.getCONSUMED_COINS()+numberoftickets*50);
                                                userInfo.setCOINS(userInfo.getCOINS()-numberoftickets*50);
                                                map1.put(Constants.COINS,userInfo.getCOINS());
                                                map1.put(Constants.COINS,userInfo.getCONSUMED_COINS());
                                            }
                                            db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).set(map1,SetOptions.merge())
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(!task.isSuccessful())
                                                                Toast.makeText(MainActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                            startActivity(new Intent(MainActivity.this, LoadingScreen.class)
                                                    .putExtra(Constants.ROOM_ID, roomid)
                                                    .putExtra(Constants.LEADER,false)
                                                    .putExtra(Constants.NUMBEROFTICKETS,numberoftickets)
                                                    .putExtra(Constants.TYPEOFCOINS,typeofcoins)
                                                    .putExtra(Constants.CUSTOMROOM,false));
                                            finish();
                                            db.collection(roomid).document((typeofcoins==0)?Constants.USER_0:Constants.USER_1).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    if(task.isSuccessful() && task.getResult()!=null){
                                                        Map<String,Object> data = task.getResult().getData();
                                                        Map<String,Object> roomdata= new HashMap<>();
                                                        roomdata.put(Constants.PLAYER,data.size());
                                                        roomdata.put(Constants.START,"0");
                                                        roomdata.put(Constants.DATA,new Date().getTime());
                                                        db.collection((typeofcoins==0)?Constants.MATCHING_0:Constants.Matching_1).document(roomid).set(roomdata);
                                                    }
                                                }
                                            });

                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });


        //Checking previous Rooms
        db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).collection(Constants.ROOMIDS).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    QuerySnapshot queryDocumentSnapshots = task.getResult();
                    if(queryDocumentSnapshots!=null && queryDocumentSnapshots.isEmpty()) return;
                    final List<DocumentSnapshot> documentSnapshots = queryDocumentSnapshots.getDocuments();
                    for(int i=0 ;i<documentSnapshots.size();i++){
                        long date = new Date(Long.parseLong(documentSnapshots.get(i).get(userInfo.getUSER_NAME()).toString())).getTime();
                        long diff = new Date().getTime()-date;
                        long diffHours = diff / (60 * 60 * 1000);
                        if(diffHours>=2) {
                            final int index = i;
                            db.collection(documentSnapshots.get(index).getId()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                 if(task.isSuccessful()){
                                     List<DocumentSnapshot> list = task.getResult().getDocuments();
                                     for(DocumentSnapshot d : list){
                                         db.collection(documentSnapshots.get(index).getId()).document(d.getId()).delete();
                                     }
                                 }
                                }
                            });
                            db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).collection(Constants.ROOMIDS).document(documentSnapshots.get(i).getId()).delete();
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(isMacthing) matching.performClick();
        if(user!=null && userInfo.getUSER_NAME()!=null) {
            db.collection(Constants.ONLINE).document(userInfo.getUSER_NAME()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    //making user offline
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void createroom(View view) {
        vibrate();
        String s = userInfo.getPHONE_NUMBER().substring(userInfo.getPHONE_NUMBER().length()-4);
        int rand =  1000 + (int)(Math.random() * 10000);
        final String room_id=s.concat(String.valueOf(rand));
        if(typeofcoins==-1) {
            Toast.makeText(MainActivity.this, "Please Select Type of Coins", Toast.LENGTH_SHORT).show();
            return;
        }
        else if(numberoftickets==0){
            Toast.makeText(MainActivity.this, "please Select number of tickets", Toast.LENGTH_SHORT).show();
            return;
        }
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.playwithfriends);
        final TextView username = dialog.findViewById(R.id.username_pwf);
        username.setText(userInfo.getUSER_NAME());
        final MaterialButton createRoom = dialog.findViewById(R.id.createroom);
        final MaterialButton joinRoom = dialog.findViewById(R.id.joinroom);
        final TextInputEditText roomId = dialog.findViewById(R.id.roomid);
        final TextInputLayout layout = dialog.findViewById(R.id.roomid_layout);
        createRoom.setText("#id "+room_id);
        joinRoom.setText("Create room");
        joinRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate();
                loading();
                Map<String,Object> map = new HashMap<>();
                map.put(Constants.ROOM_ID,room_id);
                db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER())
                        .set(map,SetOptions.merge())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @RequiresApi(api = Build.VERSION_CODES.O)
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText(MainActivity.this, "Room Created Share this with your friends", Toast.LENGTH_SHORT).show();
                                    Map<String,Object> ma = new HashMap<>();
                                    ma.put(userInfo.getUSER_NAME(),new Date().getTime());
                                    db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).collection(Constants.ROOMIDS).document(room_id).set(ma).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                        }
                                    });
                                    Map<String,Object> map1 = new HashMap<>();
                                    if(typeofcoins==1){
                                        userInfo.setCONSUMED_SUPER_COINS(userInfo.getCONSUMED_SUPER_COINS()+numberoftickets*50);
                                        userInfo.setSUPER_COINS(userInfo.getSUPER_COINS()-numberoftickets*50);
                                        map1.put(Constants.SUPER_COINS,userInfo.getSUPER_COINS());
                                        map1.put(Constants.CONSUMED_SUPER_COINS,userInfo.getCONSUMED_SUPER_COINS());
                                    }
                                    else{
                                        userInfo.setCOINS(userInfo.getCOINS()-numberoftickets*50);
                                        userInfo.setCONSUMED_COINS(userInfo.getCONSUMED_COINS()+numberoftickets*50);
                                        map1.put(Constants.COINS,userInfo.getCOINS());
                                        map1.put(Constants.CONSUMED_COINS,userInfo.getCONSUMED_COINS());
                                    }
                                    map1.put(Constants.NUMBER_OF_MATCHES,userInfo.getNUMBER_OF_MATCHES()+1);
                                    userInfo.setCOINS(userInfo.getCOINS()-50);
                                    map1.put(Constants.COINS,userInfo.getCOINS());
                                    db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).set(map1,SetOptions.merge())
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if(!task.isSuccessful())
                                                        Toast.makeText(MainActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                    Map<String,Object> m = new HashMap<>();
                                    m.put(userInfo.getPHONE_NUMBER(),Constants.LEADER+"-"+numberoftickets);
                                    db.collection(room_id).document(Constants.USERS).set(m,SetOptions.merge()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()) {
                                                startActivity(new Intent(MainActivity.this, LoadingScreen.class)
                                                        .putExtra(Constants.ROOM_ID,room_id)
                                                        .putExtra(Constants.LEADER,true)
                                                        .putExtra(Constants.NUMBEROFTICKETS,numberoftickets)
                                                        .putExtra(Constants.TYPEOFCOINS,typeofcoins)
                                                        .putExtra(Constants.CUSTOMROOM,true));
                                                Map<String,Object> ran = new HashMap<>();
                                                ran.put(Constants.TYPEOFCOINS,typeofcoins);
                                                db.collection(room_id).document(Constants.RANDOMNUMBERLIST).set(ran).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if(task.isSuccessful()) loading.cancel();
                                                    }
                                                });
                                                List<Integer> numbers= new ArrayList<>(),randomValues=new ArrayList<>();
                                                for(int i=1;i<=90;i++) numbers.add(i);
                                                for(int i=0;i<90;i++){
                                                    int ind = new Random().nextInt(numbers.size());
                                                    randomValues.add(numbers.get(ind));
                                                    numbers.remove(ind);
                                                }
                                                ran.put(Constants.RANDOMNUMBER,randomValues);
                                                Map<String,Object> start = new HashMap<>();
                                                start.put(Constants.START,"0");
                                                db.collection(room_id).document(Constants.START).set(start);
                                                db.collection(room_id).document(Constants.RANDOMNUMBERLIST).set(ran);
                                                dialog.cancel();
                                                loading.cancel();
                                                finish();
                                            }
                                        }
                                    });
                                }
                            }
                        });
            }
        });
        dialog.show();
    }

    public void joinroom(View view) {
        vibrate();
        if(typeofcoins==-1) {
            Toast.makeText(MainActivity.this, "Please Select Type of Coins", Toast.LENGTH_SHORT).show();
            return;
        }
        else if(numberoftickets==0){
            Toast.makeText(MainActivity.this,   "please Select number of tickets", Toast.LENGTH_SHORT).show();
            return;
        }
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.playwithfriends);
        final TextView username = dialog.findViewById(R.id.username_pwf);
        username.setText(userInfo.getUSER_NAME());
        final MaterialButton createRoom = dialog.findViewById(R.id.createroom);
        final MaterialButton joinRoom = dialog.findViewById(R.id.joinroom);
        final TextInputEditText roomId = dialog.findViewById(R.id.roomid);
        final TextInputLayout layout = dialog.findViewById(R.id.roomid_layout);
        layout.setVisibility(View.VISIBLE);
        roomId.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        createRoom.setVisibility(View.GONE);
        joinRoom.setText("join room");
        joinRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate();
                loading();
                final String room_id = roomId.getText().toString();
                if(room_id.equals("")) {
                    Toast.makeText(MainActivity.this, "please Enter roomid to join", Toast.LENGTH_SHORT).show();
                    loading.cancel();
                    return;
                }
                db.collection(room_id).document(Constants.USERS).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()){
                            final DocumentSnapshot documentSnapshot = task.getResult();
                            if(documentSnapshot.exists()) {
                                Map<String,Object> ma = new HashMap<>();
                                ma.put(userInfo.getUSER_NAME(),new Date().getTime());
                                db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).collection(Constants.ROOMIDS).document(room_id).set(ma).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });
                                if(task.getResult().getData().size()>=50){
                                    Toast.makeText(MainActivity.this, "Room is Filled", Toast.LENGTH_SHORT).show();
                                    loading.cancel();
                                    return;
                                }
                                db.collection(room_id).document(Constants.START).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if(task.isSuccessful()){
                                            if(task.getResult().get(Constants.START).equals("2")){
                                                Toast.makeText(MainActivity.this, "Room is in Progress, you can't enter at this time!", Toast.LENGTH_SHORT).show();
                                                return;
                                            }
                                            db.collection(room_id).document(Constants.RANDOMNUMBERLIST).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    if(task.isSuccessful()){
                                                        DocumentSnapshot snapshot= task.getResult();
                                                        if(!(Integer.parseInt(snapshot.get(Constants.TYPEOFCOINS).toString())==typeofcoins)){
                                                            Toast.makeText(MainActivity.this, "Please Select Another Type of Coins", Toast.LENGTH_SHORT).show();
                                                            loading.cancel();
                                                            dialog.cancel();
                                                            return;
                                                        }
                                                        Map<String,Object> m = new HashMap<>();
                                                        m.put(userInfo.getPHONE_NUMBER(),Constants.PLAYER+"-"+numberoftickets);
                                                        db.collection(room_id).document(Constants.USERS).set(m,SetOptions.merge()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if(task.isSuccessful()) {
                                                                    Map<String,Object> map1 = new HashMap<>();
                                                                    if(typeofcoins==1){
                                                                        userInfo.setCONSUMED_SUPER_COINS(userInfo.getCONSUMED_SUPER_COINS()+numberoftickets*50);
                                                                        userInfo.setSUPER_COINS(userInfo.getSUPER_COINS()-numberoftickets*50);
                                                                        map1.put(Constants.SUPER_COINS,userInfo.getSUPER_COINS());
                                                                        map1.put(Constants.CONSUMED_SUPER_COINS,userInfo.getSUPER_COINS());
                                                                    }
                                                                    else{
                                                                        userInfo.setCONSUMED_COINS(userInfo.getCONSUMED_COINS()+numberoftickets*50);
                                                                        userInfo.setCOINS(userInfo.getCOINS()-numberoftickets*50);
                                                                        map1.put(Constants.COINS,userInfo.getCOINS());
                                                                        map1.put(Constants.COINS,userInfo.getCONSUMED_COINS());
                                                                    }
                                                                    map1.put(Constants.NUMBER_OF_MATCHES,userInfo.getNUMBER_OF_MATCHES()+1);
                                                                    db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).set(map1,SetOptions.merge())
                                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                @Override
                                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                                    if(!task.isSuccessful())
                                                                                        Toast.makeText(MainActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                                                                                }
                                                                            });
                                                                    Toast.makeText(MainActivity.this, "Entering into Room", Toast.LENGTH_SHORT).show();
                                                                    startActivity(new Intent(MainActivity.this, LoadingScreen.class)
                                                                            .putExtra(Constants.ROOM_ID, room_id)
                                                                            .putExtra(Constants.LEADER,false)
                                                                            .putExtra(Constants.NUMBEROFTICKETS,numberoftickets)
                                                                            .putExtra(Constants.TYPEOFCOINS,typeofcoins)
                                                                            .putExtra(Constants.CUSTOMROOM,true));
                                                                    dialog.cancel();
                                                                    loading.cancel();
                                                                    finish();
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            else{
                                Toast.makeText(MainActivity.this, "Room Doesn't Exists", Toast.LENGTH_SHORT).show();
                                loading.cancel();
                            }

                        }
                    }
                });
            }
        });
        dialog.show();
    }

    public void tickets(View view) {
        vibrate();
        final ImageView imageView = findViewById(R.id.coinstype),imageView1=findViewById(R.id.numtick);
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.selection);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final CardView coin_1=dialog.findViewById(R.id.coin_1),coin_2=dialog.findViewById(R.id.coin_2),num[]={dialog.findViewById(R.id.num1),dialog.findViewById(R.id.num2),dialog.findViewById(R.id.num3)};
        final TextView[] text ={dialog.findViewById(R.id.text1),dialog.findViewById(R.id.text2),dialog.findViewById(R.id.text3)};
        if(typeofcoins==1) {
            coin_2.setElevation(0);
            coin_2.setBackgroundColor(ContextCompat.getColor(MainActivity.this,R.color.colorPrimary));
            coin_1.setElevation(20);
            coin_1.setBackgroundColor(ContextCompat.getColor(MainActivity.this,R.color.white));
        }
        if(numberoftickets!=1){
            num[numberoftickets - 1].setElevation(0);
            num[numberoftickets - 1].setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
            text[numberoftickets - 1].setTextColor(ContextCompat.getColor(MainActivity.this, R.color.white));
            num[0].setElevation(20);
            num[0].setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.white));
            text[0].setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
        }
        coin_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate();
                if(typeofcoins==1) {
                    typeofcoins=0;
                    coin_1.setElevation(0);
                    coin_1.setBackgroundColor(ContextCompat.getColor(MainActivity.this,R.color.colorPrimary));
                    coin_2.setElevation(20);
                    coin_2.setBackgroundColor(ContextCompat.getColor(MainActivity.this,R.color.white));
                    imageView.setImageResource(R.drawable.coins);
                }
            }
        });
        coin_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate();
                if(typeofcoins==0) {
                    typeofcoins=1;
                    coin_2.setElevation(0);
                    coin_2.setBackgroundColor(ContextCompat.getColor(MainActivity.this,R.color.colorPrimary));
                    coin_1.setElevation(20);
                    coin_1.setBackgroundColor(ContextCompat.getColor(MainActivity.this,R.color.white));
                    imageView.setImageResource(R.drawable.supercoins);
                }
            }
        });
        final int[] im = {R.drawable.ticket1,R.drawable.ticket2,R.drawable.ticket3};
        for(int i=0;i<3;i++) {
            final int finalI = i;
            num[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    vibrate();
                    if (numberoftickets != finalI+1) {
                        num[finalI].setElevation(0);
                        num[finalI].setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
                        text[finalI].setTextColor(ContextCompat.getColor(MainActivity.this, R.color.white));
                        num[numberoftickets - 1].setElevation(20);
                        num[numberoftickets - 1].setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.white));
                        text[numberoftickets - 1].setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
                        numberoftickets = finalI+1;
                        imageView1.setImageResource(im[finalI]);
                    }
                }
            });
        }
        dialog.show();
        dialog.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
    }

    public void coins(View view) {
        vibrate();
        tickets(new View(this));

    }

    public void spinwheel(View view) {
        vibrate();
        final int[] colors = new int[]{
                Color.parseColor("#F44336"),
                Color.parseColor("#3F51B5"),
                Color.parseColor("#009688"),
                Color.parseColor("#E91E63"),
                Color.parseColor("#2196F3"),
                Color.parseColor("#5AB963"),
                Color.parseColor("#FFC107"),
                Color.parseColor("#9C28B0"),
                Color.parseColor("#03A9F4"),
                Color.parseColor("#8BC34A"),
                Color.parseColor("#FF9800"),
                Color.parseColor("#673AB7"),
                Color.parseColor("#00BCD4"),
                Color.parseColor("#CDDC39"),
                Color.parseColor("#FF5722"),
        };
        final int[] coins_arr = new int[]{200,0,100,1000,50,500};
        List<WheelItem> wheelItems = new ArrayList<>();
        wheelItems.add(new WheelItem(Color.parseColor("#03DAC5"), BitmapFactory.decodeResource(getResources(),
                R.drawable.spincoins) , "200 "));
        wheelItems.add(new WheelItem(Color.parseColor("#293859"), BitmapFactory.decodeResource(getResources(),
                R.drawable.spincoins) , "0 "));
        wheelItems.add(new WheelItem(Color.parseColor("#03DAC5"), BitmapFactory.decodeResource(getResources(),
                R.drawable.spincoins), "100"));
        wheelItems.add(new WheelItem(Color.parseColor("#ff6b6b"), BitmapFactory.decodeResource(getResources(),
                R.drawable.spincoins), "1000"));
        wheelItems.add(new WheelItem(Color.parseColor("#293859"), BitmapFactory.decodeResource(getResources(),
                R.drawable.spincoins), "50"));
        wheelItems.add(new WheelItem(Color.parseColor("#ff6b6b"), BitmapFactory.decodeResource(getResources(),
                R.drawable.spincoins), "500"));
        final Bling bling = new Bling.Builder((ViewGroup) getWindow().getDecorView())
                .setDuration(10000)
                .setShapeCount(350)
                .setRadiusRange(5,15)
                .setRotationSpeedRange(-3f, 3f)
                .setAutoHide(true)
                .setColors(colors)
                .setSpeedRange(0.3f, 0.9f)
                .setRotationRange(90, 130)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .build();
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.spinningwheel);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final LuckyWheel lw = dialog.findViewById(R.id.lwv);
        lw.addWheelItems(wheelItems);
        MaterialButton start = dialog.findViewById(R.id.spinwheel);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n = new Random().nextInt(7);
                if(n==0 || n==7) n=1;
                lw.rotateWheelTo(n);
                System.out.println(userInfo.getCOINS()+" "+coins_arr[n-1]);
                userInfo.setCOINS(userInfo.getCOINS()+coins_arr[n-1]);
                Constants.getUserInfo().setCOINS(userInfo.getCOINS());
                Map<String,Object> m = new HashMap<>();
                m.put(Constants.COINS,userInfo.getCOINS());
                System.out.println(userInfo.getCOINS());
                db.collection(Constants.USERS).document(userInfo.getPHONE_NUMBER()).update(m);
                coins.setText(userInfo.getCOINS()+"");
            }
        });
        lw.setLuckyWheelReachTheTarget(new OnLuckyWheelReachTheTarget() {
            @Override
            public void onReachTarget() {
                final RewardedVideoAd rewardedVideoAd = new RewardedVideoAd(MainActivity.this, "765047894257227_766345924127424");
                rewardedVideoAd.setAdListener(new RewardedVideoAdListener() {
                    @Override
                    public void onError(Ad ad, AdError error) {
                        Log.e(TAG, "Rewarded video ad failed to load: " + error.getErrorMessage());
                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        Log.d(TAG, "Rewarded video ad is loaded and ready to be displayed!");
                        rewardedVideoAd.show();
                    }
                    @Override
                    public void onAdClicked(Ad ad) {
                        Log.d(TAG, "Rewarded video ad clicked!");
                    }
                    @Override
                    public void onLoggingImpression(Ad ad) {
                        Log.d(TAG, "Rewarded video ad impression logged!");
                    }
                    @Override
                    public void onRewardedVideoCompleted() {
                    }
                    @Override
                    public void onRewardedVideoClosed() {
                        Log.d(TAG, "Rewarded video ad closed!");
                    }
                });
                rewardedVideoAd.loadAd();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.cancel();
                        bling.show(BlingType.RECTANGLE);
                    }
                },1000);
            }
        });
        dialog.show();
    }

    public void redeem(View view) {
        vibrate();
        startActivity(new Intent(this, Redeem.class));
        finish();
    }

    public void profile(View view) {

        vibrate();
        startActivity(new Intent(this, Profile.class));
    }
}
