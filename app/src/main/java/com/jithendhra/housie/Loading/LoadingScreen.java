package com.jithendhra.housie.Loading;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.SetOptions;
import com.jithendhra.housie.Constants;
import com.jithendhra.housie.GameScreen.GameScreen;
import com.jithendhra.housie.MainActivity;
import com.jithendhra.housie.Player.PlayerInfo;
import com.jithendhra.housie.Player.PlayerInfoAdapter;
import com.jithendhra.housie.R;
import com.jithendhra.housie.User.UserInfo;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class LoadingScreen extends AppCompatActivity {
    FirebaseFirestore db;
    String roomid="";
    List<UserInfo> userInfos = new ArrayList<>();
    RecyclerView playerList;
    ImageView leadericon,numberoftickets;
    TextView leaderusername,loading,titole_loading,room_id;
    MaterialButton start;
    int tickets_num=0,currentprogress=0,typeofcoins,TIMELIMIT=10;
    ProgressBar loading_progress;
    boolean isLeader=false,customroom=false,gamestart=false;
    final List<PlayerInfo> playerInfos = new ArrayList<>();
    PlayerItemAdapter playerItemAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_loading_screen);
        db=FirebaseFirestore.getInstance();
        roomid=getIntent().getExtras().getString(Constants.ROOM_ID);
        isLeader=getIntent().getExtras().getBoolean(Constants.LEADER);
        tickets_num=getIntent().getExtras().getInt(Constants.NUMBEROFTICKETS);
        typeofcoins=getIntent().getExtras().getInt(Constants.TYPEOFCOINS);
        customroom=getIntent().getExtras().getBoolean(Constants.CUSTOMROOM);
        loading=findViewById(R.id.loading_sec);
        playerList=findViewById(R.id.matchingplayers);
        room_id=findViewById(R.id.roomid_loading);
        titole_loading=findViewById(R.id.titile_loading);
        loading_progress=findViewById(R.id.loading_progress);
        leadericon=findViewById(R.id.leadericon);
        ViewGroup.LayoutParams layoutParams =leadericon.getLayoutParams();
        layoutParams.width= (int) (getResources().getDisplayMetrics().heightPixels*0.15);
        layoutParams.height= (int) (getResources().getDisplayMetrics().heightPixels*0.15);
        leadericon.setLayoutParams(layoutParams);
        leaderusername=findViewById(R.id.leaderusername);
        start=findViewById(R.id.start_loading);
        numberoftickets=findViewById(R.id.noft1);
        room_id.setText(roomid+"");
        room_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, roomid);
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Room id");
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });
        playerItemAdapter = new PlayerItemAdapter(LoadingScreen.this, playerInfos);
        playerList.setLayoutManager(new LinearLayoutManager(LoadingScreen.this, LinearLayoutManager.HORIZONTAL,false));
        playerList.setAdapter(playerItemAdapter);
        if(!customroom) Matching();
        else CustomRoom();
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start.setVisibility(View.INVISIBLE);
                loading_progress.setVisibility(View.VISIBLE);
                loading.setVisibility(View.VISIBLE);
                if(isLeader){
                    Map<String, Object> map1 = new HashMap<>();
                    map1.put(Constants.START, "2");
                    db.collection(roomid).document(Constants.START).set(map1,SetOptions.merge());
                }
            }
        });

        db.collection(roomid).document(Constants.START).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if(!gamestart && documentSnapshot!=null && documentSnapshot.exists()  && documentSnapshot.get(Constants.START).equals("2")){
                    TIMELIMIT=10;
                    currentprogress=0;
                    startGame();
                }
            }
        });
    }
    public void startGame(){
        loading_progress.setVisibility(View.VISIBLE);
        new CountDownTimer(TIMELIMIT*1000,1000){
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onTick(long millisUntilFinished) {
                currentprogress++;
                loading_progress.setProgress(currentprogress*10,true);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading.setText("Game Starts in "+(TIMELIMIT-currentprogress)+" sec");
                    }
                });
            }
            @Override
            public void onFinish() {
                System.out.println("START");
                gamestart=true;
                startActivity(new Intent(LoadingScreen.this, GameScreen.class)
                        .putExtra(Constants.NUMBEROFTICKETS, tickets_num)
                        .putExtra(Constants.CUSTOMROOM,customroom)
                        .putExtra(Constants.LEADER, isLeader)
                        .putExtra(Constants.ROOM_ID, roomid));
                finish();

            }
        }.start();
    }



    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoadingScreen.this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Sorry! you can't exit while matching")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(LoadingScreen.this, MainActivity.class));
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void startTimer(){
        start.setVisibility(View.INVISIBLE);
        loading_progress.setVisibility(View.INVISIBLE);
        loading.setVisibility(View.VISIBLE);
        loading.setText("Game Will Start Until All User Join");
        room_id.setTextColor(Color.parseColor("#FF6A3B"));
        titole_loading.setText("Matching Ends in");
        room_id.setText("00:59");
        room_id.setCompoundDrawables(null,null,null,null);
        final int[] min={0};
        final int[] sec={60};
        new CountDownTimer(6*1000,1000){
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onTick(long millisUntilFinished) {
                if(sec[0]==0) {
                    min[0]--;
                    sec[0]=59;
                }
                if(sec[0]>=10) room_id.setText("0"+min[0]+":"+sec[0]);
                else room_id.setText("0"+min[0]+":0"+sec[0]);
                sec[0]--;
            }
            @Override
            public void onFinish() {
                room_id.setText("00:00");
                db.collection(roomid).document(Constants.START).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            DocumentSnapshot d = task.getResult();
                            if (d.get(Constants.START).equals("0")) {
                                Map<String, Object> map1 = new HashMap<>();
                                Toast.makeText(LoadingScreen.this, "Started", Toast.LENGTH_SHORT).show();
                                map1.put(Constants.START, "2");
                                db.collection(roomid).document(Constants.START).set(map1, SetOptions.merge());
                                db.collection((typeofcoins == 0) ? Constants.MATCHING_0 : Constants.Matching_1).document(roomid).set(map1, SetOptions.merge());
                            }
                        }
                    }
                });
            }
        }.start();
    }

    private void CustomRoom() {
        if(isLeader) {
            start.setVisibility(View.VISIBLE);
            loading_progress.setVisibility(View.INVISIBLE);
            loading.setVisibility(View.INVISIBLE);
        }
        else {
            start.setVisibility(View.INVISIBLE);
            loading_progress.setVisibility(View.INVISIBLE);
            loading.setVisibility(View.VISIBLE);
            loading.setText("Game Will Start Until All User Join");
        }
        db.collection(roomid).document(Constants.USERS).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                final List<UserInfo> userInfos1= new ArrayList<>();
                final List<PlayerInfo> playerInfos = new ArrayList<>();
                Map<String,Object> map = documentSnapshot.getData();
                if(map!=null) {
                    for (Map.Entry<String, Object> entry : map.entrySet()) {
                        final String[] st = entry.getValue().toString().split("[-]");
                        db.collection(Constants.USERS).document(entry.getKey()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot documentSnapshot1 = task.getResult();
                                    userInfos1.add(new UserInfo(
                                            documentSnapshot1.get(Constants.USER_NAME).toString(),
                                            documentSnapshot1.get(Constants.PHONE_NUMBER).toString(),
                                            documentSnapshot1.get(Constants.ROOM_ID).toString(),
                                            Float.parseFloat(documentSnapshot1.get(Constants.LUCK_RATIO).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.COINS).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.SUPER_COINS).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.NUMBER_OF_MATCHES).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.WIN_COINS).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.WIN_SUPER_COINS).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.CONSUMED_COINS).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.CONSUMED_SUPER_COINS).toString()),
                                            documentSnapshot1.get(Constants.AVATAR).toString(),
                                            documentSnapshot1.get(Constants.REFERRAL).toString()
                                    ));
                                    if(st[0].equals(Constants.LEADER)){
                                        Picasso.get().load(userInfos1.get(userInfos1.size()-1).getAVATAR()).into(leadericon);
                                        leaderusername.setText(userInfos1.get(userInfos1.size()-1).getUSER_NAME());
                                        switch (Integer.parseInt(st[1])){
                                            case 1:numberoftickets.setImageDrawable(ContextCompat.getDrawable(LoadingScreen.this,R.drawable.ticket1));break;
                                            case 2:numberoftickets.setImageDrawable(ContextCompat.getDrawable(LoadingScreen.this,R.drawable.ticket2));break;
                                            case 3:numberoftickets.setImageDrawable(ContextCompat.getDrawable(LoadingScreen.this,R.drawable.ticket3));break;
                                        }
                                    }
                                    else {
                                        playerInfos.add(new PlayerInfo(userInfos1.get(userInfos1.size() - 1).getAVATAR(),userInfos1.get(userInfos1.size() - 1).getUSER_NAME(),Integer.parseInt(st[1])));
                                        PlayerItemAdapter playerItemAdapter = new PlayerItemAdapter(LoadingScreen.this, playerInfos);
                                        playerList.setLayoutManager(new LinearLayoutManager(LoadingScreen.this, LinearLayoutManager.HORIZONTAL,false));
                                        playerList.setAdapter(playerItemAdapter);
                                    }
                                }
                            }
                        });
                    }
                    userInfos.addAll(userInfos1);
                }
            }
        });
    }

    private void Matching() {
        startTimer();
        botsCreation();
        db.collection(roomid).document((typeofcoins==0)?Constants.USER_0:Constants.USER_1).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable final DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                final List<UserInfo> userInfos1= new ArrayList<>();
                Map<String,Object> map = documentSnapshot.getData();
                if(map!=null) {
                    int i=0;
                    for (final Map.Entry<String, Object> entry : map.entrySet()) {
                        if(entry.getKey().equals(Constants.getUserInfo().getPHONE_NUMBER())){
                            Picasso.get().load(Constants.getUserInfo().getAVATAR()).into(leadericon);
                            leaderusername.setText(Constants.getUserInfo().getUSER_NAME());
                            switch (Integer.parseInt(entry.getValue().toString())){
                                case 1:numberoftickets.setImageDrawable(ContextCompat.getDrawable(LoadingScreen.this,R.drawable.ticket1));break;
                                case 2:numberoftickets.setImageDrawable(ContextCompat.getDrawable(LoadingScreen.this,R.drawable.ticket2));break;
                                case 3:numberoftickets.setImageDrawable(ContextCompat.getDrawable(LoadingScreen.this,R.drawable.ticket3));break;
                            }
                        }
                        else{
                            db.collection(Constants.USERS).document(entry.getKey()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        DocumentSnapshot documentSnapshot1 = task.getResult();
                                        userInfos1.add(new UserInfo(
                                                documentSnapshot1.get(Constants.USER_NAME).toString(),
                                                documentSnapshot1.get(Constants.PHONE_NUMBER).toString(),
                                                documentSnapshot1.get(Constants.ROOM_ID).toString(),
                                                Float.parseFloat(documentSnapshot1.get(Constants.LUCK_RATIO).toString()),
                                                Long.parseLong(documentSnapshot1.get(Constants.COINS).toString()),
                                                Long.parseLong(documentSnapshot1.get(Constants.SUPER_COINS).toString()),
                                                Long.parseLong(documentSnapshot1.get(Constants.NUMBER_OF_MATCHES).toString()),
                                                Long.parseLong(documentSnapshot1.get(Constants.WIN_COINS).toString()),
                                                Long.parseLong(documentSnapshot1.get(Constants.WIN_SUPER_COINS).toString()),
                                                Long.parseLong(documentSnapshot1.get(Constants.CONSUMED_COINS).toString()),
                                                Long.parseLong(documentSnapshot1.get(Constants.CONSUMED_SUPER_COINS).toString()),
                                                documentSnapshot1.get(Constants.AVATAR).toString(),
                                                documentSnapshot1.get(Constants.REFERRAL).toString()
                                        ));
                                        playerInfos.add(new PlayerInfo(userInfos1.get(userInfos1.size() - 1).getAVATAR(),userInfos1.get(userInfos1.size() - 1).getUSER_NAME(),Integer.parseInt(entry.getValue().toString())));
                                        playerItemAdapter.update(playerInfos);
                                    }
                                }
                            });
                        }
                        i++;
                    }
                    userInfos.addAll(userInfos1);
                }
            }
        });

        db.collection(roomid).document(Constants.BOTS).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                if(value.exists()){
                    Map<String,Object> map = value.getData();
                    for (final Map.Entry<String, Object> entry : map.entrySet()){
                        db.collection(Constants.BOTS).document(entry.getKey()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                DocumentSnapshot documentSnapshot = task.getResult();
                                playerInfos.add(new PlayerInfo(documentSnapshot.get(Constants.AVATAR).toString(),documentSnapshot.get(Constants.USER_NAME).toString(),Integer.parseInt(entry.getValue().toString())));
                                playerItemAdapter.update(playerInfos);
                            }
                        });
                    }
                }
            }
        });

    }

    private void botsCreation() {
        int s = new Random().nextInt(3);
        for(int i=0;i<=s;i++) {
            Map<String, Object> m = new HashMap<>();
            int r = new Random().nextInt(100);
            m.put(Constants.BOTS+r,1+new Random().nextInt(2));
            db.collection(roomid).document(Constants.BOTS).set(m,SetOptions.merge());
        }
    }

    @Override
    protected void onPause() {
        if(!customroom && !gamestart) {
            Map<String, Object> map = new HashMap<>();
            map.put(Constants.getUserInfo().getPHONE_NUMBER(), FieldValue.delete());
            db.collection(roomid).document((typeofcoins == 0) ? Constants.USER_0 : Constants.USER_1).update(map);
            db.collection(roomid).document((typeofcoins == 0) ? Constants.USER_0 : Constants.USER_1).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        Map<String, Object> data = task.getResult().getData();
                        Map<String, Object> roomdata = new HashMap<>();
                        roomdata.put(Constants.PLAYER, data.size());
                        db.collection((typeofcoins == 0) ? Constants.MATCHING_0 : Constants.Matching_1).document(roomid).set(roomdata, SetOptions.merge());
                    }
                }
            });
        }
        super.onPause();
    }


}