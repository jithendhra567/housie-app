package com.jithendhra.housie.Loading;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jithendhra.housie.Player.PlayerInfo;
import com.jithendhra.housie.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PlayerItemAdapter  extends RecyclerView.Adapter<PlayerItemAdapter.ViewHolder>{
    private List<PlayerInfo> playerInfos;
    private Context context;
    public PlayerItemAdapter(Context context,List<PlayerInfo> playerInfos) {
        this.playerInfos = playerInfos;
        this.context = context;
    }
    public void update(List<PlayerInfo> playerInfos){
        this.playerInfos=playerInfos;
        notifyDataSetChanged();
    }
    @Override
    public PlayerItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.loading_useritem, parent, false);
        PlayerItemAdapter.ViewHolder viewHolder = new PlayerItemAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PlayerItemAdapter.ViewHolder holder, final int position) {
        Picasso.get().load(playerInfos.get(position).getImage()).into(holder.icon);
        ViewGroup.LayoutParams layoutParams = holder.icon.getLayoutParams();
        layoutParams.width= (int) (context.getResources().getDisplayMetrics().heightPixels*0.15);
        layoutParams.height= (int) (context.getResources().getDisplayMetrics().heightPixels*0.15);
        holder.icon.setLayoutParams(layoutParams);
        holder.username.setText(playerInfos.get(position).getTitle());
        switch (playerInfos.get(position).getNumberoftickets()){
            case 1:holder.tickets.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ticket1));break;
            case 2:holder.tickets.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ticket2));break;
            case 3:holder.tickets.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ticket3));break;
        }
    }
    @Override
    public int getItemCount() {
        return playerInfos.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon,tickets;
        TextView username;
        ViewHolder(View itemView) {
            super(itemView);
            this.icon =  itemView.findViewById(R.id.usericon_item);
            this.tickets=itemView.findViewById(R.id.noft);
            this.username=itemView.findViewById(R.id.username_item);
        }
    }
}
