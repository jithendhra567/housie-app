package com.jithendhra.housie.Payments;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.jithendhra.housie.Constants;
import com.jithendhra.housie.MainActivity;
import com.jithendhra.housie.R;
import com.shreyaspatil.EasyUpiPayment.EasyUpiPayment;
import com.shreyaspatil.EasyUpiPayment.listener.PaymentStatusListener;
import com.shreyaspatil.EasyUpiPayment.model.PaymentApp;
import com.shreyaspatil.EasyUpiPayment.model.TransactionDetails;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class SuperCoinsPayments extends AppCompatActivity  implements PaymentStatusListener {

    CardView phonepe,googlepay,bhim,patym;
    TextInputEditText amount;
    MaterialCardView payment;
    TextView currentsupercoins;
    int coinstoadd=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_super_coins_payments);
        phonepe=findViewById(R.id.phonepe);
        bhim=findViewById(R.id.bhim);
        patym=findViewById(R.id.paytm);
        googlepay=findViewById(R.id.googlepay);
        payment=findViewById(R.id.payment);
        amount=findViewById(R.id.amount);
        currentsupercoins=findViewById(R.id.currentsupercoins);

        currentsupercoins.setText(Constants.getUserInfo().getSUPER_COINS()+"");

        phonepe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Transcationid="phonepe"+Constants.getUserInfo().getPHONE_NUMBER()+""+new Random().nextInt(1000),money;
                if(amount.getText().toString().length()==0){
                    Toast.makeText(SuperCoinsPayments.this, "Please Enter Some Amount", Toast.LENGTH_SHORT).show();
                }
                try{
                    money  = amount.getText().toString()+".00";
                    Float f = Float.parseFloat(money);
                }
                catch (Exception e){
                    Toast.makeText(SuperCoinsPayments.this, "Please Enter Correct Amount", Toast.LENGTH_SHORT).show();
                    return;
                }
                final EasyUpiPayment easyUpiPayment = new EasyUpiPayment.Builder()
                        .with(SuperCoinsPayments.this)
                        .setPayeeVpa("8074467501@ybl")
                        .setPayeeName("jthendhra")
                        .setTransactionId(Transcationid)
                        .setTransactionRefId(Transcationid)
                        .setDescription("Buying Super Coins")
                        .setAmount(money)
                        .build();
                easyUpiPayment.setDefaultPaymentApp(PaymentApp.PHONE_PE);
                easyUpiPayment.setPaymentStatusListener(SuperCoinsPayments.this);
                easyUpiPayment.startPayment();
            }
        });

        googlepay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Transcationid="googlepay"+Constants.getUserInfo().getPHONE_NUMBER()+""+new Random().nextInt(1000),money;
                if(amount.getText().toString().length()==0){
                    Toast.makeText(SuperCoinsPayments.this, "Please Enter Some Amount", Toast.LENGTH_SHORT).show();
                    return;
                }
                try{
                    money  = amount.getText().toString()+".00";
                    Float f = Float.parseFloat(money);
                }
                catch (Exception e){
                    Toast.makeText(SuperCoinsPayments.this, "Please Enter Correct Amount", Toast.LENGTH_SHORT).show();
                    return;
                }
                final EasyUpiPayment easyUpiPayment = new EasyUpiPayment.Builder()
                        .with(SuperCoinsPayments.this)
                        .setPayeeVpa("kvr20011972@okhdfcbank")
                        .setPayeeName("jthendhra")
                        .setTransactionId(Transcationid)
                        .setTransactionRefId(Transcationid)
                        .setDescription("Buying Super Coins")
                        .setAmount(money)
                        .build();
                easyUpiPayment.setDefaultPaymentApp(PaymentApp.GOOGLE_PAY);
                easyUpiPayment.setPaymentStatusListener(SuperCoinsPayments.this);
                easyUpiPayment.startPayment();
            }
        });

        patym.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Transcationid="paytm"+Constants.getUserInfo().getPHONE_NUMBER()+""+new Random().nextInt(1000),money;
                if(amount.getText().toString().length()==0){
                    Toast.makeText(SuperCoinsPayments.this, "Please Enter Some Amount", Toast.LENGTH_SHORT).show();
                }
                try{
                    money  = amount.getText().toString()+".00";
                    Float f = Float.parseFloat(money);
                }
                catch (Exception e){
                    Toast.makeText(SuperCoinsPayments.this, "Please Enter Correct Amount", Toast.LENGTH_SHORT).show();
                    return;
                }
                final EasyUpiPayment easyUpiPayment = new EasyUpiPayment.Builder()
                        .with(SuperCoinsPayments.this)
                        .setPayeeVpa("8074467501@ybl")
                        .setPayeeName("jthendhra")
                        .setTransactionId(Transcationid)
                        .setTransactionRefId(Transcationid)
                        .setDescription("Buying Super Coins")
                        .setAmount(money)
                        .build();
                easyUpiPayment.setDefaultPaymentApp(PaymentApp.PAYTM);
                easyUpiPayment.setPaymentStatusListener(SuperCoinsPayments.this);
                easyUpiPayment.startPayment();
            }
        });

        bhim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Transcationid="bhim"+Constants.getUserInfo().getPHONE_NUMBER()+""+new Random().nextInt(1000),money;
                if(amount.getText().toString().length()==0){
                    Toast.makeText(SuperCoinsPayments.this, "Please Enter Some Amount", Toast.LENGTH_SHORT).show();
                }
                try{
                    money  = amount.getText().toString()+".00";
                    Float f = Float.parseFloat(money);
                }
                catch (Exception e){
                    Toast.makeText(SuperCoinsPayments.this, "Please Enter Correct Amount", Toast.LENGTH_SHORT).show();
                    return;
                }
                final EasyUpiPayment easyUpiPayment = new EasyUpiPayment.Builder()
                        .with(SuperCoinsPayments.this)
                        .setPayeeVpa("8074467501@ybl")
                        .setPayeeName("jthendhra")
                        .setTransactionId(Transcationid)
                        .setTransactionRefId(Transcationid)
                        .setDescription("Buying Super Coins")
                        .setAmount(money)
                        .build();
                easyUpiPayment.setDefaultPaymentApp(PaymentApp.BHIM_UPI);
                easyUpiPayment.setPaymentStatusListener(SuperCoinsPayments.this);
                easyUpiPayment.startPayment();
            }
        });

        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Transcationid="default"+Constants.getUserInfo().getPHONE_NUMBER()+""+new Random().nextInt(1000),money;
                if(amount.getText().toString().length()==0){
                    Toast.makeText(SuperCoinsPayments.this, "Please Enter Some Amount", Toast.LENGTH_SHORT).show();
                }
                try{
                    money  = amount.getText().toString()+".00";
                    Float f = Float.parseFloat(money);
                }
                catch (Exception e){
                    Toast.makeText(SuperCoinsPayments.this, "Please Enter Correct Amount", Toast.LENGTH_SHORT).show();
                    return;
                }
                final EasyUpiPayment easyUpiPayment = new EasyUpiPayment.Builder()
                        .with(SuperCoinsPayments.this)
                        .setPayeeVpa("8074467501@ybl")
                        .setPayeeName("jthendhra")
                        .setTransactionId(Transcationid)
                        .setTransactionRefId(Transcationid)
                        .setDescription("Buying Super Coins")
                        .setAmount(money)
                        .build();
                easyUpiPayment.setDefaultPaymentApp(PaymentApp.NONE);
                easyUpiPayment.setPaymentStatusListener(SuperCoinsPayments.this);
                easyUpiPayment.startPayment();
            }
        });
    }

    @Override
    public void onTransactionCompleted(TransactionDetails transactionDetails) {
        // Transaction Completed
        coinstoadd=(int)Float.parseFloat(transactionDetails.getAmount());
        Log.d("TransactionDetails", transactionDetails.toString());
    }

    @Override
    public void onTransactionSuccess() {
        // Payment Success
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
        Map<String,Object> add = new HashMap<>();
        add.put(Constants.SUPER_COINS,coinstoadd);
        FirebaseFirestore.getInstance().collection(Constants.USERS).document(Constants.getUserInfo().getPHONE_NUMBER()).set(add, SetOptions.merge()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(SuperCoinsPayments.this, "Coins Added", Toast.LENGTH_SHORT).show();
                    Constants.getUserInfo().setSUPER_COINS(Constants.getUserInfo().getSUPER_COINS()+coinstoadd);
                    currentsupercoins.setText(Constants.getUserInfo().getSUPER_COINS()+"");
                }
            }
        });
    }

    @Override
    public void onTransactionSubmitted() {
        // Payment Pending
        Toast.makeText(this, "Pending | Submitted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTransactionFailed() {
        // Payment Failed
        Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTransactionCancelled() {
        // Payment Cancelled by User
        Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAppNotFound() {
        Toast.makeText(this, "App Not Found", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }
}