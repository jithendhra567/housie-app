package com.jithendhra.housie.Profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.RewardedVideoAd;
import com.facebook.ads.RewardedVideoAdListener;
import com.jithendhra.housie.Constants;
import com.jithendhra.housie.MainActivity;
import com.jithendhra.housie.R;
import com.squareup.picasso.Picasso;

public class Profile extends AppCompatActivity {

    LinearLayout personalinfo, experience;
    ImageView usericon;
    TextView personalinfobtn, experiencebtn,username,coins,supercoins,referral,wincoins,winsupercoins,luckratio,numofmatches,concoins,consupercoins;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_profile);
        final String TAG ="Ads";
        final RewardedVideoAd rewardedVideoAd = new RewardedVideoAd(this, "765047894257227_766996860728997");
        rewardedVideoAd.setAdListener(new RewardedVideoAdListener() {
            @Override
            public void onError(Ad ad, AdError error) {
                Log.e(TAG, "Rewarded video ad failed to load: " + error.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Log.d(TAG, "Rewarded video ad is loaded and ready to be displayed!");
                rewardedVideoAd.show();
            }
            @Override
            public void onAdClicked(Ad ad) {
                Log.d(TAG, "Rewarded video ad clicked!");
            }
            @Override
            public void onLoggingImpression(Ad ad) {
                Log.d(TAG, "Rewarded video ad impression logged!");
            }
            @Override
            public void onRewardedVideoCompleted() {
            }
            @Override
            public void onRewardedVideoClosed() {
                Log.d(TAG, "Rewarded video ad closed!");
            }
        });
        rewardedVideoAd.loadAd();


        wincoins=findViewById(R.id.wincoins);
        winsupercoins=findViewById(R.id.winsupercoins);
        luckratio=findViewById(R.id.luckratio);
        numofmatches=findViewById(R.id.numofmatches);
        concoins=findViewById(R.id.consumedcoins);
        consupercoins=findViewById(R.id.consumedsupercoins);
        referral=findViewById(R.id.referral_profile);
        usericon=findViewById(R.id.profile_usericon);
        username=findViewById(R.id.username_profile);
        coins=findViewById(R.id.coins_profile);
        supercoins=findViewById(R.id.supercoins_profile);
        personalinfo = findViewById(R.id.personalinfo);
        experience = findViewById(R.id.experience);
        personalinfobtn = findViewById(R.id.personalinfobtn);
        experiencebtn = findViewById(R.id.experiencebtn);
        /*making personal info visible*/
        personalinfo.setVisibility(View.VISIBLE);
        experience.setVisibility(View.GONE);

        wincoins.setText(Constants.getUserInfo().getWIN_COINS()+"");
        winsupercoins.setText(Constants.getUserInfo().getWIN_SUPER_COINS()+"");
        luckratio.setText(Constants.getUserInfo().getLUCK_RATIO()+"");
        numofmatches.setText(Constants.getUserInfo().getNUMBER_OF_MATCHES()+"");
        concoins.setText(Constants.getUserInfo().getCONSUMED_COINS()+"");
        consupercoins.setText(Constants.getUserInfo().getCONSUMED_SUPER_COINS()+"");
        referral.setText(Constants.getUserInfo().getREFERRAL());
        coins.setText(Constants.getUserInfo().getCOINS()+"");
        supercoins.setText(Constants.getUserInfo().getSUPER_COINS()+"");
        username.setText(Constants.getUserInfo().getUSER_NAME());
        Picasso.get().load(Constants.getUserInfo().getAVATAR()).into(usericon);

        personalinfobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                personalinfo.setVisibility(View.VISIBLE);
                experience.setVisibility(View.GONE);
                personalinfobtn.setTextColor(getResources().getColor(R.color.blue));
                experiencebtn.setTextColor(getResources().getColor(R.color.grey));

            }
        });

        experiencebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                personalinfo.setVisibility(View.GONE);
                experience.setVisibility(View.VISIBLE);
                personalinfobtn.setTextColor(getResources().getColor(R.color.grey));
                experiencebtn.setTextColor(getResources().getColor(R.color.blue));

            }
        });
        experiencebtn.performClick();

    }

    public void share(View view) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Here is the referral code for housie";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,Constants.getUserInfo().getREFERRAL());
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public void copy(View view) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(Constants.REFERRAL, Constants.getUserInfo().getREFERRAL());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, "copied", Toast.LENGTH_SHORT).show();
    }
}