package com.jithendhra.housie.GameScreen;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.SetOptions;
import com.jithendhra.housie.Board.BoardAdapter;
import com.jithendhra.housie.Board.BoardItem;
import com.jithendhra.housie.Board.RecentNumberAdapter;
import com.jithendhra.housie.Chat.ChatAdapter;
import com.jithendhra.housie.Chat.ChatItem;
import com.jithendhra.housie.Constants;
import com.jithendhra.housie.EndScreen.EndScreen;
import com.jithendhra.housie.MainActivity;
import com.jithendhra.housie.Player.PlayerInfo;
import com.jithendhra.housie.Player.PlayerInfoAdapter;
import com.jithendhra.housie.R;
import com.jithendhra.housie.Ticket.Ticket;
import com.jithendhra.housie.Ticket.TicketAdapter;
import com.jithendhra.housie.User.UserInfo;
import com.wajahatkarim3.easyflipview.EasyFlipView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import me.samlss.bling.Bling;
import me.samlss.bling.BlingType;


public class GameScreen extends AppCompatActivity {
    final  List<BoardItem> boardItems= new ArrayList<>();
    RecentNumberAdapter recentNumberAdapter;
    Dialog loading;
    final ArrayList<Integer> noofTickets=new ArrayList<>();
    final Map<String,String> winners = new HashMap<>(),items=new HashMap<>(),prizepool=new HashMap<>();
    final ArrayList<String> usernames=new ArrayList<>(),usericons = new ArrayList<>();
    final List<ChatItem> chatItemList = new ArrayList<>();
    final List<Integer> randomValues = new ArrayList<>(),currentrecentnumbers= new ArrayList<>();
    String room_id="";
    TextView counter;
    PlayerInfoAdapter playerInfoAdapter;
    final List<PlayerInfo> playerInfos1 = new ArrayList<>();
    LinearLayout prizepoolinfo,gamestartlayout;
    TextView gamestartsinsec,sec,second,totalcoins_view,reward_title,winnerusername,unreadmessges;
    int currentrandonindex=0,gamestarts=5;
    boolean isCustomRoom = false;
    RecyclerView playerList,recentNumbers;
    FirebaseFirestore database;
    ImageView coinicon,rewardwinner;
    String ffwinner = Constants.NOWINNER,tlwinner=Constants.NOWINNER,mlwinner=Constants.NOWINNER,blwinner=Constants.NOWINNER,housiewinner=Constants.NOWINNER;
    List<Integer> winningindexs=new ArrayList<>();
    String currentwinningitem=Constants.NOWINNER;
    int numberoftickers,typeofcoins,totalcoins,currentwiningindex,unreadMessagesCout=0;
    ImageView boardbutton;
    MaterialButton claim;
    String chatBot="";
    CardView unreadBadge;
    int toplinereward,middlelinereward,bottomlinereward,firstfivereward,housiereward;
    FloatingActionButton chatting;
    EasyFlipView easyFlipView;
    Ticket[][] tickets;
    List<UserInfo> userInfos=new ArrayList<>();
    MaterialButtonToggleGroup ticketnumber;
    int[] colors = new int[]{
            Color.parseColor("#F44336"),
            Color.parseColor("#3F51B5"),
            Color.parseColor("#009688"),
            Color.parseColor("#E91E63"),
            Color.parseColor("#2196F3"),
            Color.parseColor("#5AB963"),
            Color.parseColor("#FFC107"),
            Color.parseColor("#9C28B0"),
            Color.parseColor("#03A9F4"),
            Color.parseColor("#8BC34A"),
            Color.parseColor("#FF9800"),
            Color.parseColor("#673AB7"),
            Color.parseColor("#00BCD4"),
            Color.parseColor("#CDDC39"),
            Color.parseColor("#FF5722"),
    };
    Bling bling;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game_screen);

        init();

        if(typeofcoins==1)coinicon.setImageResource(R.drawable.supercoins);
        else coinicon.setImageResource(R.drawable.coins);
        startgame();
        database.collection(room_id).document(Constants.RANDOMNUMBERLIST).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot documentSnapshot1 = task.getResult();
                    List<Integer> list = (List<Integer>) documentSnapshot1.get(Constants.RANDOMNUMBER);
                    randomValues.addAll(list);
                }
            }
        });
        playerInfoAdapter = new PlayerInfoAdapter(GameScreen.this,playerInfos1);
        playerList.setLayoutManager(new LinearLayoutManager(GameScreen.this, LinearLayoutManager.HORIZONTAL,false));
        playerList.setAdapter(playerInfoAdapter);
        getusersdata();

        prizepooldeatils();

        generatingtickets();

        claimMethodd();

        chattingmethod();
    }

    private void init() {
        Constants.timer=70;
        counter=findViewById(R.id.count);
        playerList = findViewById(R.id.playerslist);
        prizepoolinfo=findViewById(R.id.prizepoolinfo);
        reward_title=findViewById(R.id.rewardtitle);
        rewardwinner=findViewById(R.id.claimrewardslist);
        winnerusername=findViewById(R.id.username_winner);
        easyFlipView=findViewById(R.id.cardflip);
        claim = findViewById(R.id.claim);
        chatting=findViewById(R.id.chatting);
        database=FirebaseFirestore.getInstance();
        loading = new Dialog(this);
        loading.setContentView(R.layout.loading);
        gamestartlayout=findViewById(R.id.startgamelayout);
        recentNumbers = findViewById(R.id.recentnumbers);
        unreadBadge=findViewById(R.id.unreadbadge);
        unreadmessges=findViewById(R.id.unreadmessage);
        sec=findViewById(R.id.sec);
        coinicon=findViewById(R.id.coin_image);
        second=findViewById(R.id.second);
        totalcoins_view=findViewById(R.id.totalcoins);
        gamestartsinsec = findViewById(R.id.startgameinsec);
        boardbutton=findViewById(R.id.boardbutton);
        //bling
        bling = new Bling.Builder((ViewGroup) getWindow().getDecorView())
                .setDuration(10000)
                .setShapeCount(350)
                .setRadiusRange(5,15)
                .setRotationSpeedRange(-3f, 3f)
                .setAutoHide(true)
                .setColors(colors)
                .setSpeedRange(0.3f, 0.9f)
                .setRotationRange(90, 130)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .build();
        room_id=getIntent().getExtras().getString(Constants.ROOM_ID);
        numberoftickers=getIntent().getExtras().getInt(Constants.NUMBEROFTICKETS);
        typeofcoins=getIntent().getExtras().getInt(Constants.TYPEOFCOINS);
        isCustomRoom=getIntent().getExtras().getBoolean(Constants.CUSTOMROOM);

    }

    private void claimMethodd() {
        ticketnumber = findViewById(R.id.toggleButton);
        ticketnumber.setSingleSelection(true);
        if(numberoftickers==2)findViewById(R.id.claim3).setVisibility(View.GONE);
        else if(numberoftickers==1){
            findViewById(R.id.claim2).setVisibility(View.GONE);
            findViewById(R.id.claim3).setVisibility(View.GONE);
        }
        final int[] ticketnumbers = {-1};
        ticketnumber.addOnButtonCheckedListener(new MaterialButtonToggleGroup.OnButtonCheckedListener() {
            @Override
            public void onButtonChecked(MaterialButtonToggleGroup group, int checkedId, boolean isChecked) {
                switch (group.getCheckedButtonId()){
                    case R.id.claim1: ticketnumbers[0]=0;  break;
                    case R.id.claim2: ticketnumbers[0]=1;  break;
                    case R.id.claim3: ticketnumbers[0]=2;  break;
                    default:
                }
            }
        });
        final int[] line = {-1};
        final MaterialButtonToggleGroup linereward = findViewById(R.id.housiereward);
        linereward.setSingleSelection(true);
        linereward.addOnButtonCheckedListener(new MaterialButtonToggleGroup.OnButtonCheckedListener() {
            @Override
            public void onButtonChecked(MaterialButtonToggleGroup group, int checkedId, boolean isChecked) {
                switch (group.getCheckedButtonId()){
                    case R.id.topline: line[0]=0;  break;
                    case R.id.middleline: line[0]=1;  break;
                    case R.id.bottomline: line[0]=2;  break;
                    default: line[0]=-1;
                }
            }
        });
        MaterialButtonToggleGroup housiereward = findViewById(R.id.housiereward2);
        housiereward.setSingleSelection(true);
        final int[] housie = {-1};
        housiereward.addOnButtonCheckedListener(new MaterialButtonToggleGroup.OnButtonCheckedListener() {
            @Override
            public void onButtonChecked(MaterialButtonToggleGroup group, int checkedId, boolean isChecked) {
                if(group.getCheckedButtonId()==R.id.firstfive) housie[0]=0;
                else if(group.getCheckedButtonId()==R.id.housie) housie[0]=1;
                else housie[0]=-1;
            }
        });

        database.collection(room_id).document(Constants.CLAIMREWARD).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if(documentSnapshot!=null && documentSnapshot.exists()){
                    Map<String,Object> m = documentSnapshot.getData();
                    if(m!=null)
                        for (Map.Entry<String,Object> entry : m.entrySet()){
                            String[] winner = entry.getValue().toString().split("[-]");
                            if(entry.getKey().equals(Constants.FIRSTFIVE)) {
                                reward_title.setText(Constants.FIRSTFIVE);
                                if(ffwinner.equals(Constants.NOWINNER)) ffwinner=winner[0];
                                else ffwinner=ffwinner+"\n"+winner[0];
                                winningindexs.add(Integer.parseInt(winner[1]));
                                currentwinningitem=Constants.FIRSTFIVE;
                                currentrandonindex=Integer.parseInt(winner[1]);
                                if(!winners.containsKey(winner[0])) winners.put(winner[0],Constants.FIRSTFIVE);
                                else winners.put(winner[0],winners.get(winner[0])+"\n"+Constants.FIRSTFIVE);
                                if(!items.containsKey(Constants.FIRSTFIVE)) items.put(Constants.FIRSTFIVE,"1");
                                else items.put(Constants.FIRSTFIVE,items.get(Constants.FIRSTFIVE)+1);
                            }
                            else if(entry.getKey().equals(Constants.TOPLINE)) {
                                reward_title.setText(Constants.TOPLINE);
                                if(tlwinner.equals(Constants.NOWINNER)) tlwinner=winner[0];
                                else tlwinner=tlwinner+"\n"+winner[0];
                                winningindexs.add(Integer.parseInt(winner[1]));
                                currentwinningitem=Constants.TOPLINE;
                                currentrandonindex=Integer.parseInt(winner[1]);
                                if(!winners.containsKey(winner[0])) winners.put(winner[0],Constants.TOPLINE);
                                else winners.put(winner[0],winners.get(winner[0])+"\n"+Constants.TOPLINE);
                                if(!items.containsKey(Constants.TOPLINE)) items.put(Constants.TOPLINE,"1");
                                else items.put(Constants.TOPLINE,items.get(Constants.TOPLINE)+1);
                            }
                            else if(entry.getKey().equals(Constants.MIDDLELINE)) {
                                reward_title.setText(Constants.MIDDLELINE);
                                if(mlwinner.equals(Constants.NOWINNER)) mlwinner=winner[0];
                                else mlwinner=mlwinner+"\n"+winner[0];
                                winningindexs.add(Integer.parseInt(winner[1]));
                                currentwinningitem=Constants.MIDDLELINE;
                                currentrandonindex=Integer.parseInt(winner[1]);
                                if(!winners.containsKey(winner[0])) winners.put(winner[0],Constants.MIDDLELINE);
                                else winners.put(winner[0],winners.get(winner[0])+"\n"+Constants.MIDDLELINE);
                                if(!items.containsKey(Constants.MIDDLELINE)) items.put(Constants.MIDDLELINE,"1");
                                else items.put(Constants.MIDDLELINE,items.get(Constants.MIDDLELINE)+1);
                            }
                            else if(entry.getKey().equals(Constants.BOTTOMLINE)) {
                                reward_title.setText(Constants.BOTTOMLINE);
                                if(blwinner.equals(Constants.NOWINNER)) blwinner=winner[0];
                                else blwinner=blwinner+"\n"+winner[0];
                                winningindexs.add(Integer.parseInt(winner[1]));
                                currentwinningitem=Constants.BOTTOMLINE;
                                currentrandonindex=Integer.parseInt(winner[1]);
                                if(!winners.containsKey(winner[0])) winners.put(winner[0],Constants.BOTTOMLINE);
                                else winners.put(winner[0],winners.get(winner[0])+"\n"+Constants.BOTTOMLINE);
                                if(!items.containsKey(Constants.BOTTOMLINE)) items.put(Constants.BOTTOMLINE,"1");
                                else items.put(Constants.BOTTOMLINE,items.get(Constants.BOTTOMLINE)+1);
                            }
                            else if(entry.getKey().equals(Constants.HOUSIE)) {
                                reward_title.setText(Constants.HOUSIE);
                                if(housiewinner.equals(Constants.NOWINNER)) housiewinner=winner[0];
                                else housiewinner=housiewinner+"\n"+winner[0];
                                winningindexs.add(Integer.parseInt(winner[1]));
                                currentwinningitem=Constants.HOUSIE;
                                currentrandonindex=Integer.parseInt(winner[1]);
                                if(!winners.containsKey(winner[0])) winners.put(winner[0],Constants.HOUSIE);
                                else winners.put(winner[0],winners.get(winner[0])+"\n"+Constants.HOUSIE);
                                if(!items.containsKey(Constants.HOUSIE)) items.put(Constants.HOUSIE,"1");
                                else items.put(Constants.HOUSIE,items.get(Constants.HOUSIE)+1);
                                bling.show(BlingType.RECTANGLE);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(GameScreen.this, EndScreen.class)
                                                .putExtra(Constants.CUSTOMROOM,isCustomRoom)
                                                .putExtra(Constants.ROOM_ID, room_id)
                                                .putExtra(Constants.TYPEOFCOINS,typeofcoins)
                                                .putStringArrayListExtra(Constants.PLAYER,usernames)
                                                .putIntegerArrayListExtra(Constants.NUMBEROFTICKETS,noofTickets)
                                                .putStringArrayListExtra(Constants.AVATAR,usericons));
                                        finish();
                                    }
                                },2000);
                            }
                            rewardwinner.setVisibility(View.GONE);
                            easyFlipView.flipTheView();
                            winnerusername.setText(winner[0]);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    rewardwinner.setVisibility(View.VISIBLE);
                                }
                            },3400);
                        }
                    Constants.setItems(winners);
                    Constants.setWinners(winners);
                    System.out.println(winners);
                }
            }
        });

        //claim reward list
        rewardwinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(GameScreen.this);
                dialog.setContentView(R.layout.claimlayout);
                int width = (int) (getResources().getDisplayMetrics().widthPixels*0.9);
                int height = (int) (getResources().getDisplayMetrics().heightPixels*0.75);
                dialog.getWindow().setLayout(width,height);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();
                TextView ff=dialog.findViewById(R.id.firstfivewinner),
                        tl=dialog.findViewById(R.id.toplinewinner),
                        ml=dialog.findViewById(R.id.middlelinewinner),
                        bl=dialog.findViewById(R.id.bottomlinewinner),
                        h=dialog.findViewById(R.id.housiewinner);
                ff.setText(ffwinner);
                tl.setText(tlwinner);
                ml.setText(mlwinner);
                bl.setText(blwinner);
                h.setText(housiewinner);
            }
        });

        claim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean[] tickdata = new boolean[27];
                int[] ticketdata = new int[27];
                String s = "";
                if(ticketnumbers[0]==-1){
                    Toast.makeText(GameScreen.this, "please Select Ticket ", Toast.LENGTH_SHORT).show();
                    return;
                }
                for (int i = 0; i < 27; i++) {
                    tickdata[i] = tickets[ticketnumbers[0]][i].isTick();
                    ticketdata[i] = tickets[ticketnumbers[0]][i].getNumber();
                }
                if(line[0]==-1 && housie[0]==-1) {
                    Toast.makeText(GameScreen.this, "Please select Any thing to get Rewarded", Toast.LENGTH_SHORT).show();
                    return;
                }
                else if(line[0]!=-1 && housie[0]!=-1){
                    Toast.makeText(GameScreen.this, "Please Select One Reward", Toast.LENGTH_SHORT).show();
                    return;
                }
                else if(line[0] != -1) {
                    if (checkLine(tickdata, ticketdata, line[0]*9, (line[0]+1)*9)){
                        Map<String,Object> m= new HashMap<>();
                        if(line[0]==0) m.put(Constants.TOPLINE,Constants.getUserInfo().getUSER_NAME()+"-"+currentrandonindex);
                        else if(line[0]==1) m.put(Constants.MIDDLELINE,Constants.getUserInfo().getUSER_NAME()+"-"+currentrandonindex);
                        else m.put(Constants.BOTTOMLINE,Constants.getUserInfo().getUSER_NAME()+"-"+currentrandonindex);
                        bling.show(BlingType.RECTANGLE);
                        database.collection(room_id).document(Constants.CLAIMREWARD).set(m).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful())

                                    Toast.makeText(GameScreen.this, "Success", Toast.LENGTH_SHORT).show();
                            }
                        });
                        line[0]=-1;

                    }
                    else
                        Toast.makeText(GameScreen.this, "Warning : Wrong Claim", Toast.LENGTH_SHORT).show();
                }
                else {
                    if(check(tickdata,ticketdata,housie[0])) {
                        Toast.makeText(GameScreen.this, "Success", Toast.LENGTH_SHORT).show();
                        bling.show(BlingType.RECTANGLE);
                        Map<String,Object> m= new HashMap<>();
                        if(housie[0]==0) m.put(Constants.FIRSTFIVE,Constants.getUserInfo().getUSER_NAME()+"-"+currentrandonindex);
                        else m.put(Constants.HOUSIE,Constants.getUserInfo().getUSER_NAME()+"-"+currentrandonindex);
                        database.collection(room_id).document(Constants.CLAIMREWARD).set(m).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){}
                            }
                        });
                        housie[0]=-1;
                    }
                    else Toast.makeText(GameScreen.this, "Warning : Wrong Claim", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void chattingmethod() {
        final ChatAdapter chatAdapter = new ChatAdapter(GameScreen.this,chatItemList);
        database.collection(room_id).document(Constants.CHATROOM).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable final DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if(documentSnapshot.exists()){
                    if(chatItemList.size()>0 && chatItemList.get(chatItemList.size()-1).getUsername().equals(documentSnapshot.get(Constants.USER_NAME).toString()))
                        chatItemList.get(chatItemList.size()-1).setMessage(chatItemList.get(chatItemList.size()-1).getMessage()+"\n"+documentSnapshot.get(Constants.CHATMESSAGE));
                    else chatItemList.add(new ChatItem(documentSnapshot.get(Constants.AVATAR).toString(),
                            documentSnapshot.get(Constants.USER_NAME).toString(),
                            documentSnapshot.get(Constants.CHATMESSAGE).toString()));
                    chatAdapter.update(chatItemList);
                    unreadMessagesCout++;
                    unreadBadge.setVisibility(View.VISIBLE);
                    unreadmessges.setText(unreadMessagesCout+"");
                }
            }
        });


        chatting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(GameScreen.this);
                dialog.setContentView(R.layout.chat);
                chatting.setBackgroundColor(ContextCompat.getColor(GameScreen.this, R.color.white));
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                int width = (int) (getResources().getDisplayMetrics().widthPixels*0.9);
                int height = (int) (getResources().getDisplayMetrics().heightPixels*0.65);
                dialog.getWindow().setLayout(width,height);
                dialog.show();
                final EditText message = dialog.findViewById(R.id.msg);
                final RecyclerView chatview = dialog.findViewById(R.id.chatView);
                ImageView send = dialog.findViewById(R.id.send);
                chatview.setLayoutManager(new LinearLayoutManager(GameScreen.this));
                chatview.setAdapter(chatAdapter);
                if(chatItemList.size()>3)chatview.smoothScrollToPosition(chatItemList.size()-1);
                send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(message.getText().toString().length()==0) {
                            Toast.makeText(GameScreen.this, "Write Something", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Map<String,Object> m = new HashMap<>();
                        m.put(Constants.AVATAR,Constants.getUserInfo().getAVATAR());
                        m.put(Constants.USER_NAME,Constants.getUserInfo().getUSER_NAME());
                        m.put(Constants.CHATMESSAGE,message.getText().toString());
                        message.setText("");
                        Toast.makeText(GameScreen.this, "sending", Toast.LENGTH_SHORT).show();
                        database.collection(room_id).document(Constants.CHATROOM).set(m).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(chatItemList.size()>3)chatview.smoothScrollToPosition(chatItemList.size()-1);
                            }
                        });
                    }
                });
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        unreadBadge.setVisibility(View.INVISIBLE);
                        unreadMessagesCout=0;
                    }
                });
            }
        });
    }

    private void generatingtickets() {
        tickets = new Ticket[numberoftickers][27];
        List<List<Integer>> numbers = new ArrayList<>();
        for(int m=0;m<9;m++){
            List<Integer> l = new ArrayList<>();
            for(int k=0;k<10;k++) l.add(m*10+k);
            numbers.add(l);
        }
        String[] seq = {"011010110","011011001","101010011","101101100","101100011","110110010","110001011","010110110","100110011","101001101","100101011","110101010","100011011","110011010","101010101","001011011","011010101","100101101","110001101","101101010","110110100","110100110","110101001","010101101","110010011","011010011","010011011","101011001","001101101","110100101","101010110","101101001","101100110","010110011","011001101","100110110","110010101","010101011","110011001","101011010","101100101","011001011","100110101","010110101","110110001","101001011","001101011","110101100","110100011","011011010","110010110"};
        for(int k = 0;k<numberoftickers;k++){
            String prev = "000000000";
            List<String> seqList = new ArrayList<>(Arrays.asList(seq));
            Ticket[] ticketItems = new Ticket[27];
            for(int i=0;i<3;i++) {
                Random random = new Random();
                String s = getPerfectRow(seqList,prev);
                for(int j=0;j<9;j++) {
                    if (s.charAt(j) == '1' && numbers.get(j).size()>0) {
                        int in = random.nextInt(numbers.get(j).size());
                        ticketItems[i * 9 + j] = new Ticket(numbers.get(j).get(in),false);
                        numbers.get(j).remove(in);
                    }
                    else ticketItems[i*9+j] = new Ticket(0,false);
                }
                prev=s;
            }
            tickets[k] = ticketItems;
        }
        RecyclerView recyclerView1= findViewById(R.id.ticket1);
        ViewGroup.LayoutParams layoutParams = recyclerView1.getLayoutParams();
        layoutParams.height= (int) (getResources().getDisplayMetrics().widthPixels*0.30);
        recyclerView1.setLayoutParams(layoutParams);
        TicketAdapter ticketAdapter1 = new TicketAdapter(this,tickets[0]);
        recyclerView1.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.HORIZONTAL));
        recyclerView1.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
        recyclerView1.setLayoutManager(new GridLayoutManager(this,9));
        recyclerView1.setAdapter(ticketAdapter1);
        if(numberoftickers>=2) {
            RecyclerView recyclerView2 = findViewById(R.id.ticket2);
            recyclerView2.setLayoutParams(layoutParams);
            TicketAdapter ticketAdapter2 = new TicketAdapter(this, tickets[1]);
            recyclerView2.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.HORIZONTAL));
            recyclerView2.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
            recyclerView2.setLayoutManager(new GridLayoutManager(this, 9));
            recyclerView2.setAdapter(ticketAdapter2);
        }
        RecyclerView recyclerView3= findViewById(R.id.ticket3);
        if(numberoftickers==3) {
            recyclerView3.setLayoutParams(layoutParams);
            TicketAdapter ticketAdapter3 = new TicketAdapter(this, tickets[2]);
            recyclerView3.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.HORIZONTAL));
            recyclerView3.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
            recyclerView3.setLayoutManager(new GridLayoutManager(this, 9));
            recyclerView3.setAdapter(ticketAdapter3);
        }
    }

    private void prizepooldeatils() {
        prizepoolinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(GameScreen.this);
                dialog.setContentView(R.layout.prizepool);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();
                ImageView ffi=dialog.findViewById(R.id.ffrewardicon),
                        tli=dialog.findViewById(R.id.tlrewardicon),
                        mli=dialog.findViewById(R.id.mlrewardicon),
                        bli=dialog.findViewById(R.id.blrewardicon),
                        hi=dialog.findViewById(R.id.hrewardicon);
                if(typeofcoins==0){
                    ffi.setImageResource(R.drawable.coins);
                    tli.setImageResource(R.drawable.coins);
                    mli.setImageResource(R.drawable.coins);
                    bli.setImageResource(R.drawable.coins);
                    hi.setImageResource(R.drawable.coins);
                }
                else{
                    ffi.setImageResource(R.drawable.supercoins);
                    tli.setImageResource(R.drawable.supercoins);
                    mli.setImageResource(R.drawable.supercoins);
                    bli.setImageResource(R.drawable.supercoins);
                    hi.setImageResource(R.drawable.supercoins);
                }
                TextView ff=dialog.findViewById(R.id.firstfivereward),
                        tl=dialog.findViewById(R.id.toplinereward),
                        ml=dialog.findViewById(R.id.middlelinereward),
                        bl=dialog.findViewById(R.id.bottomlinereward),
                        h=dialog.findViewById(R.id.housiereward_pool);
                ff.setText(firstfivereward+"");
                tl.setText(toplinereward+"");
                ml.setText(middlelinereward+"");
                bl.setText(bottomlinereward+"");
                h.setText(housiereward+"");
            }
        });
    }

    private void getusersdata() {
        database.collection(room_id).document((isCustomRoom)?Constants.USERS:(typeofcoins==0)?Constants.USER_0:Constants.USER_1).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable final DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                final List<UserInfo> userInfos1= new ArrayList<>();
                //intilizing
                usernames.addAll(new ArrayList<String>());
                usericons.addAll(new ArrayList<String>());
                noofTickets.addAll(new ArrayList<Integer>());
                Map<String,Object> map = documentSnapshot.getData();
                totalcoins=0;
                final int[] temp = {0};
                if(map!=null) {
                    for (Map.Entry<String, Object> entry : map.entrySet()) {
                        int st =-1;
                        if(entry.getValue().toString().length()<=1)   st = Integer.parseInt(entry.getValue().toString());
                        else  st = Integer.parseInt(entry.getValue().toString().split("[-]")[1]);
                        final int finalSt = st;
                        database.collection(Constants.USERS).document(entry.getKey()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot documentSnapshot1 = task.getResult();
                                    userInfos1.add(new UserInfo(
                                            documentSnapshot1.get(Constants.USER_NAME).toString(),
                                            documentSnapshot1.get(Constants.PHONE_NUMBER).toString(),
                                            documentSnapshot1.get(Constants.ROOM_ID).toString(),
                                            Float.parseFloat(documentSnapshot1.get(Constants.LUCK_RATIO).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.COINS).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.SUPER_COINS).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.NUMBER_OF_MATCHES).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.WIN_COINS).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.WIN_SUPER_COINS).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.CONSUMED_COINS).toString()),
                                            Long.parseLong(documentSnapshot1.get(Constants.CONSUMED_SUPER_COINS).toString()),
                                            documentSnapshot1.get(Constants.AVATAR).toString(),
                                            documentSnapshot1.get(Constants.REFERRAL).toString()
                                    ));
                                    usernames.add(userInfos1.get(userInfos1.size()-1).getUSER_NAME());
                                    usericons.add(userInfos1.get(userInfos1.size()-1).getAVATAR());
                                    noofTickets.add(finalSt);
                                    playerInfos1.add(new PlayerInfo(userInfos1.get(userInfos1.size()-1).getAVATAR(),userInfos1.get(userInfos1.size()-1).getUSER_NAME(), finalSt));
                                    playerInfoAdapter.update(playerInfos1);
                                }
                            }
                        });
                        temp[0] +=st*50;
                    }
                    userInfos.addAll(userInfos1);
                }
                database.collection(room_id).document(Constants.BOTS).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()&& task.getResult()!=null){
                            Map<String,Object> map = task.getResult().getData();
                            if(map!=null) {
                                for (final Map.Entry<String, Object> entry : map.entrySet()) {
                                    database.collection(Constants.BOTS).document(entry.getKey()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                            DocumentSnapshot documentSnapshot = task.getResult();
                                            if (chatBot.equals("")) chatBot = entry.getKey();
                                            playerInfos1.add(new PlayerInfo(documentSnapshot.get(Constants.AVATAR).toString(), documentSnapshot.get(Constants.USER_NAME).toString(), Integer.parseInt(entry.getValue().toString())));
                                            playerInfoAdapter.update(playerInfos1);
                                            temp[0] = Integer.parseInt(entry.getValue().toString()) * 50;
                                        }
                                    });
                                }
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        final String[] a = {"hi", "hello", "hey"};
                                        database.collection(Constants.BOTS).document(chatBot).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    Map<String, Object> m = new HashMap<>();
                                                    m.put(Constants.AVATAR, task.getResult().get(Constants.AVATAR));
                                                    m.put(Constants.USER_NAME, task.getResult().get(Constants.USER_NAME));
                                                    m.put(Constants.CHATMESSAGE, a[new Random().nextInt(a.length)]);
                                                    database.collection(room_id).document(Constants.CHATROOM).set(m);
                                                }
                                            }
                                        });
                                    }
                                }, 1000);
                            }
                        }
                    }
                });
                totalcoins=Math.max(temp[0],totalcoins);
                //Calculating poolprize
                totalcoins=totalcoins-(int)(0.1*totalcoins);
                toplinereward=(int)(totalcoins*0.15);
                middlelinereward=(int)(totalcoins*0.15);
                bottomlinereward=(int)(totalcoins*0.15);
                firstfivereward=(int)(totalcoins*0.2);
                housiereward=(int)(totalcoins*0.3);
                totalcoins_view.setText(""+totalcoins);
                Constants.setPrizepool(new HashMap<String, String>(){ { put(Constants.FIRSTFIVE,firstfivereward+"");
                        put(Constants.TOPLINE,toplinereward+"");
                        put(Constants.MIDDLELINE,middlelinereward+"");
                        put(Constants.TOPLINE,toplinereward+"");
                        put(Constants.BOTTOMLINE,bottomlinereward+"");
                        put(Constants.HOUSIE,housiereward+"");
                    }
                });
            }
        });


    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(GameScreen.this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Do you want really to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(GameScreen.this, MainActivity.class));
                        Map<String,Object> map = new HashMap<>();
                        map.put(Constants.ROOM_ID,"0");
                        database.collection(Constants.USERS).document(Constants.getUserInfo().getPHONE_NUMBER())
                                .set(map, SetOptions.merge())
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful())
                                            Toast.makeText(GameScreen.this, "Room Closed", Toast.LENGTH_SHORT).show();
                                    }
                                });
                        final Map<String,Object> m = new HashMap<>();
                        m.put(Constants.getUserInfo().getPHONE_NUMBER(), FieldValue.delete());
                        database.collection(room_id).document((isCustomRoom)?Constants.USERS:(typeofcoins==0)?Constants.USER_0:Constants.USER_1).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                DocumentSnapshot documentSnapshot = task.getResult();
                                if(!documentSnapshot.exists() || documentSnapshot.getData().size()==1) {
                                    database.collection(room_id).document((isCustomRoom)?Constants.USERS:(typeofcoins==0)?Constants.USER_0:Constants.USER_1).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                        //document deletion
                                        }
                                    });
                                    database.collection(room_id).document(Constants.START).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            //document deletion
                                        }
                                    });
                                    database.collection(room_id).document(Constants.RANDOMNUMBERLIST).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            //document deletion
                                        }
                                    });
                                }
                                else{
                                    database.collection(room_id).document((isCustomRoom)?Constants.USERS:(typeofcoins==0)?Constants.USER_0:Constants.USER_1).update(m).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            //Data Removed from Room
                                        }
                                    });
                                }
                            }
                        });
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void loading() {
        loading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loading.show();
        loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false);
    }
    public boolean check(boolean[] b, int[] a,int id){
        int c=0;
        for(int i=0;i<a.length;i++){
            if(b[i] && currentrecentnumbers.contains(a[i])) c++;
        }
        return (id==0)?c==5:c==15;
    }


    public  boolean checkLine(boolean[] b,int[] a,int i,int j) {
        int m=0,l=0;
        for (int k = i; k < j; k++) {
            if(a[k]!=0) l++;
            if (a[k] != 0 && currentrecentnumbers.contains(a[k]) && b[k]) m++;
        }
        return m==l;
    }

    public  String getPerfectRow(List<String> rows,String prev){
        Random random = new Random();
        String s;
        int c=0;
        do {
            c=0;
            int index = random.nextInt(rows.size());
            s = rows.get(index);
            for (int i = 0; i < 9; i++){
                if (prev.charAt(i)=='1' && prev.charAt(i) == s.charAt(i)) c++;
            }
        }while (c>2);
        return s;
    }

    public void generateRandomNumbers(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(currentrandonindex!=0 && currentrandonindex%10==0) Constants.timer=Constants.timer-3;
                if(currentwiningindex<=currentrandonindex){
                    if(currentwinningitem.equals(Constants.FIRSTFIVE)) findViewById(R.id.firstfive).setVisibility(View.GONE);
                    else if(currentwinningitem.equals(Constants.TOPLINE)) findViewById(R.id.topline).setVisibility(View.GONE);
                    else if(currentwinningitem.equals(Constants.MIDDLELINE)) findViewById(R.id.middleline).setVisibility(View.GONE);
                    else if(currentwinningitem.equals(Constants.BOTTOMLINE)) findViewById(R.id.bottomline).setVisibility(View.GONE);
                    if(currentwinningitem.equals(Constants.HOUSIE)) findViewById(R.id.housie).setVisibility(View.GONE);
                }
                boardItems.add(new BoardItem(Integer.parseInt(String.valueOf(randomValues.get(currentrandonindex))),false));
                currentrecentnumbers.add(Integer.parseInt(String.valueOf(randomValues.get(currentrandonindex))));
                counter.setText(currentrandonindex+"/90");
                currentrandonindex++;
                if(boardItems.size()==6){
                    boardItems.remove(0);
                    boardItems.get(boardItems.size()-2).setTick(true);
                    recentNumberAdapter.update(boardItems,true);
                    recentNumbers.smoothScrollToPosition(boardItems.size()-1);
                }
                else {
                    recentNumberAdapter.update(boardItems,false);
                    recentNumbers.smoothScrollToPosition(boardItems.size()-1);
                }
                if(currentrandonindex<randomValues.size()) generateRandomNumbers();
                else{
                    startActivity(new Intent(GameScreen.this, EndScreen.class)
                            .putExtra(Constants.CUSTOMROOM,isCustomRoom)
                            .putExtra(Constants.ROOM_ID, room_id)
                            .putExtra(Constants.TYPEOFCOINS,typeofcoins)
                            .putStringArrayListExtra(Constants.PLAYER,usernames)
                            .putIntegerArrayListExtra(Constants.NUMBEROFTICKETS,noofTickets)
                            .putStringArrayListExtra(Constants.AVATAR,usericons));
                    finish();
                }
            }
        },(int)(15000*(Constants.timer/100.0)));
    }

    public void boarddialog(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.gameboard);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView boardNumbers = dialog.findViewById(R.id.board);
        int width = (int) (getResources().getDisplayMetrics().widthPixels*0.95);
        int height = (int) (getResources().getDisplayMetrics().heightPixels*0.65);
        dialog.getWindow().setLayout(width,height);
        List<BoardItem> boardItems = new ArrayList<>();
        boardItems.add(new BoardItem(90,false));
        for(int i=1;i<90;i++) boardItems.add(new BoardItem(i,false));
        if(currentrecentnumbers.size()>0) for(int i=0;i<currentrecentnumbers.size();i++)  {
            if(currentrecentnumbers.get(i)==90) boardItems.get(0).setTick(true);
            else boardItems.get(currentrecentnumbers.get(i)).setTick(true);
        }
        BoardAdapter boardAdapter = new BoardAdapter(this,boardItems);
        boardNumbers.setLayoutManager(new GridLayoutManager(this,10));
        boardNumbers.setAdapter(boardAdapter);
        dialog.show();
        CardView cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
    }

    public void startgame() {
        final Timer timer = new Timer();
        Map<String,Object> m = new HashMap<>();
        m.put(Constants.START,"1");
        database.collection(room_id).document(Constants.START).set(m).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                gamestartlayout.setVisibility(View.VISIBLE);
                second.setVisibility(View.VISIBLE);
                sec.setVisibility(View.VISIBLE);
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        if(gamestarts==0){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    gamestartlayout.setVisibility(View.GONE);
                                    recentNumbers.setVisibility(View.VISIBLE);
                                    boardbutton.setVisibility(View.VISIBLE);
                                    boardItems.add(new BoardItem(Integer.parseInt(String.valueOf(randomValues.get(currentrandonindex))),false));
                                    currentrecentnumbers.add(Integer.parseInt(String.valueOf(randomValues.get(currentrandonindex))));
                                    currentrandonindex++;
                                    recentNumberAdapter = new RecentNumberAdapter(GameScreen.this,boardItems);
                                    recentNumbers.setLayoutManager(new LinearLayoutManager(GameScreen.this,LinearLayoutManager.HORIZONTAL,false));
                                    recentNumbers.setAdapter(recentNumberAdapter);
                                    generateRandomNumbers();
                                }
                            });
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                gamestartsinsec.setText("Game Starts in ");
                                second.setText("SEC");
                                sec.setText(gamestarts+"");
                            }
                        });
                        gamestarts--;
                    }
                },0,1000);

            }
        });
        }
}
