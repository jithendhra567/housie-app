package com.jithendhra.housie.Board;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.jithendhra.housie.R;

import java.util.List;

public class BoardAdapter extends RecyclerView.Adapter<BoardAdapter.ViewHolder>{
    private List<BoardItem> boardItems;
    private Context context;
    public BoardAdapter(Context context, List<BoardItem> boardItems) {
        this.boardItems = boardItems;
        this.context = context;
    }

    public void update(List<BoardItem> boardItems){
        this.boardItems=boardItems;
        notifyItemChanged(boardItems.size()-1);
    }

    @Override
    public BoardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.boarditem, parent, false);
        BoardAdapter.ViewHolder viewHolder = new BoardAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(final BoardAdapter.ViewHolder holder, final int position) {
        if(boardItems.get(position).isTick()) {
            holder.number.setBackground(ContextCompat.getDrawable(context,boardItems.get(position).getImage2()));
            FrameLayout.LayoutParams layout = (FrameLayout.LayoutParams) holder.number.getLayoutParams();
            layout.setMargins(0,0,0,0);
            layout.width = (int) (context.getResources().getDisplayMetrics().widthPixels*0.075);
            layout.height = (int) (context.getResources().getDisplayMetrics().heightPixels*0.039);
            holder.number.setLayoutParams(layout);
        }
        else {
            holder.number.setBackground(ContextCompat.getDrawable(context,boardItems.get(position).getImage1()));
            FrameLayout.LayoutParams layout = (FrameLayout.LayoutParams) holder.number.getLayoutParams();
            layout.setMargins(10,10,10,10);
            layout.width = (int) (context.getResources().getDisplayMetrics().widthPixels*0.055);
            layout.height = (int) (context.getResources().getDisplayMetrics().heightPixels*0.029);
            holder.number.setLayoutParams(layout);
        }
    }

    @Override
    public int getItemCount() { return boardItems.size(); }

    static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout number;
        ViewHolder(View itemView) {
            super(itemView);
            this.number=itemView.findViewById(R.id.boardnumber);
        }
    }
}


