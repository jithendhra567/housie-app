package com.jithendhra.housie.Board;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;


import com.jithendhra.housie.Constants;
import com.jithendhra.housie.R;

import java.util.List;

public class RecentNumberAdapter extends RecyclerView.Adapter<RecentNumberAdapter.ViewHolder>{
    private List<BoardItem> boardItems;
    private Context context;
    public RecentNumberAdapter(Context context, List<BoardItem> boardItems) {
            this.boardItems = boardItems;
            this.context = context;
    }

    public void update(List<BoardItem> boardItems,Boolean remove){
        this.boardItems=boardItems;
        if(!remove)notifyItemChanged(boardItems.size()-1);
        else notifyDataSetChanged();
    }

    @Override
    public RecentNumberAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.recentnumber, parent, false);
        RecentNumberAdapter.ViewHolder viewHolder = new RecentNumberAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(final RecentNumberAdapter.ViewHolder holder, final int position) {
        holder.number.setImageResource(boardItems.get(position).getImage1());
        if(boardItems.get(position).isTick()) holder.progressBar.setProgress(100);
        else {
            for (int i = 0; i <= 100; i++) {
                final int finalI = i;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        holder.progressBar.setProgress(finalI);
                        if (holder.progressBar.getProgress() == 100)
                            boardItems.get(position).setTick(true);
                    }
                }, (int)((i * 150)*(Constants.timer/100.0)));
            }
        }
    }

    @Override
    public int getItemCount() { return boardItems.size(); }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView number;
        ProgressBar progressBar;
        ViewHolder(View itemView) {
            super(itemView);
            this.number=itemView.findViewById(R.id.boardnumber);
            this.progressBar=itemView.findViewById(R.id.progressnumber);
        }
    }
}

