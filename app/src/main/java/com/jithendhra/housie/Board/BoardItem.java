package com.jithendhra.housie.Board;


import com.jithendhra.housie.R;

public class BoardItem {
    int[] drawableImage = new int[]{R.drawable.n0,R.drawable.n1,R.drawable.n2,R.drawable.n3,R.drawable.n4,R.drawable.n5,R.drawable.n6,R.drawable.n7,R.drawable.n8,R.drawable.n9,R.drawable.n10,R.drawable.n11,R.drawable.n12,R.drawable.n13,R.drawable.n14,R.drawable.n15,R.drawable.n16,R.drawable.n17,R.drawable.n18,R.drawable.n19,R.drawable.n20,R.drawable.n21,R.drawable.n22,R.drawable.n23,R.drawable.n24,R.drawable.n25,R.drawable.n26,R.drawable.n27,R.drawable.n28,R.drawable.n29,R.drawable.n30,R.drawable.n31,R.drawable.n32,R.drawable.n33,R.drawable.n34,R.drawable.n35,R.drawable.n36,R.drawable.n37,R.drawable.n38,R.drawable.n39,R.drawable.n40,R.drawable.n41,R.drawable.n42,R.drawable.n43,R.drawable.n44,R.drawable.n45,R.drawable.n46,R.drawable.n47,R.drawable.n48,R.drawable.n49,R.drawable.n50,R.drawable.n51,R.drawable.n52,R.drawable.n53,R.drawable.n54,R.drawable.n55,R.drawable.n56,R.drawable.n57,R.drawable.n58,R.drawable.n59,R.drawable.n60,R.drawable.n61,R.drawable.n62,R.drawable.n63,R.drawable.n64,R.drawable.n65,R.drawable.n66,R.drawable.n67,R.drawable.n68,R.drawable.n69,R.drawable.n70,R.drawable.n71,R.drawable.n72,R.drawable.n73,R.drawable.n74,R.drawable.n75,R.drawable.n76,R.drawable.n77,R.drawable.n78,R.drawable.n79,R.drawable.n80,R.drawable.n81,R.drawable.n82,R.drawable.n83,R.drawable.n84,R.drawable.n85,R.drawable.n86,R.drawable.n87,R.drawable.n88,R.drawable.n89,R.drawable.n90};
    int[] drawableImage2 = new int[]{R.drawable.m90,R.drawable.m1,R.drawable.m2,R.drawable.m3,R.drawable.m4,R.drawable.m5,R.drawable.m6,R.drawable.m7,R.drawable.m8,R.drawable.m9,R.drawable.m10,R.drawable.m11,R.drawable.m12,R.drawable.m13,R.drawable.m14,R.drawable.m15,R.drawable.m16,R.drawable.m17,R.drawable.m18,R.drawable.m19,R.drawable.m20,R.drawable.m21,R.drawable.m22,R.drawable.m23,R.drawable.m24,R.drawable.m25,R.drawable.m26,R.drawable.m27,R.drawable.m28,R.drawable.m29,R.drawable.m30,R.drawable.m31,R.drawable.m32,R.drawable.m33,R.drawable.m34,R.drawable.m35,R.drawable.m36,R.drawable.m37,R.drawable.m38,R.drawable.m39,R.drawable.m40,R.drawable.m41,R.drawable.m42,R.drawable.m43,R.drawable.m44,R.drawable.m45,R.drawable.m46,R.drawable.m47,R.drawable.m48,R.drawable.m49,R.drawable.m50,R.drawable.m51,R.drawable.m52,R.drawable.m53,R.drawable.m54,R.drawable.m55,R.drawable.m56,R.drawable.m57,R.drawable.m58,R.drawable.m59,R.drawable.m60,R.drawable.m61,R.drawable.m62,R.drawable.m63,R.drawable.m64,R.drawable.m65,R.drawable.m66,R.drawable.m67,R.drawable.m68,R.drawable.m69,R.drawable.m70,R.drawable.m71,R.drawable.m72,R.drawable.m73,R.drawable.m74,R.drawable.m75,R.drawable.m76,R.drawable.m77,R.drawable.m78,R.drawable.m79,R.drawable.m80,R.drawable.m81,R.drawable.m82,R.drawable.m83,R.drawable.m84,R.drawable.m85,R.drawable.m86,R.drawable.m87,R.drawable.m88,R.drawable.m89};
    int number;
    boolean tick;
    public BoardItem(int number,boolean tick){
        this.number=number;
        this.tick=tick;
    }

    public boolean isTick() {
        return tick;
    }

    public void setTick(boolean tick) {
        this.tick = tick;
    }

    public int getImage1() {
        return drawableImage[number];
    }


    public int getImage2() {
        if(number==90) return drawableImage2[0];
        return drawableImage2[number];
    }

    public void setImage(int number) {
        this.number = number;
    }
    public void setImage2(int number) {
        this.number = number;
    }
}
