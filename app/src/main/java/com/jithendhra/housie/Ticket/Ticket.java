package com.jithendhra.housie.Ticket;


import com.jithendhra.housie.R;

public class Ticket {
    int[] drawableImage = new int[]{R.drawable.n0,R.drawable.n1,R.drawable.n2,R.drawable.n3,R.drawable.n4,R.drawable.n5,R.drawable.n6,R.drawable.n7,R.drawable.n8,R.drawable.n9,R.drawable.n10,R.drawable.n11,R.drawable.n12,R.drawable.n13,R.drawable.n14,R.drawable.n15,R.drawable.n16,R.drawable.n17,R.drawable.n18,R.drawable.n19,R.drawable.n20,R.drawable.n21,R.drawable.n22,R.drawable.n23,R.drawable.n24,R.drawable.n25,R.drawable.n26,R.drawable.n27,R.drawable.n28,R.drawable.n29,R.drawable.n30,R.drawable.n31,R.drawable.n32,R.drawable.n33,R.drawable.n34,R.drawable.n35,R.drawable.n36,R.drawable.n37,R.drawable.n38,R.drawable.n39,R.drawable.n40,R.drawable.n41,R.drawable.n42,R.drawable.n43,R.drawable.n44,R.drawable.n45,R.drawable.n46,R.drawable.n47,R.drawable.n48,R.drawable.n49,R.drawable.n50,R.drawable.n51,R.drawable.n52,R.drawable.n53,R.drawable.n54,R.drawable.n55,R.drawable.n56,R.drawable.n57,R.drawable.n58,R.drawable.n59,R.drawable.n60,R.drawable.n61,R.drawable.n62,R.drawable.n63,R.drawable.n64,R.drawable.n65,R.drawable.n66,R.drawable.n67,R.drawable.n68,R.drawable.n69,R.drawable.n70,R.drawable.n71,R.drawable.n72,R.drawable.n73,R.drawable.n74,R.drawable.n75,R.drawable.n76,R.drawable.n77,R.drawable.n78,R.drawable.n79,R.drawable.n80,R.drawable.n81,R.drawable.n82,R.drawable.n83,R.drawable.n84,R.drawable.n85,R.drawable.n86,R.drawable.n87,R.drawable.n88,R.drawable.n89,R.drawable.n90};
    int[] drawableImage2= new int[]{R.drawable.n0,R.drawable.t1, R.drawable.t2, R.drawable.t3, R.drawable.t4, R.drawable.t5, R.drawable.t6, R.drawable.t7, R.drawable.t8, R.drawable.t9, R.drawable.t10, R.drawable.t11, R.drawable.t12, R.drawable.t13, R.drawable.t14, R.drawable.t15, R.drawable.t16, R.drawable.t17, R.drawable.t18, R.drawable.t19, R.drawable.t20, R.drawable.t21, R.drawable.t22, R.drawable.t23, R.drawable.t24, R.drawable.t25, R.drawable.t26, R.drawable.t27, R.drawable.t28, R.drawable.t29, R.drawable.t30, R.drawable.t31, R.drawable.t32, R.drawable.t33, R.drawable.t34, R.drawable.t35, R.drawable.t36, R.drawable.t37, R.drawable.t38, R.drawable.t39, R.drawable.t40, R.drawable.t41, R.drawable.t42, R.drawable.t43, R.drawable.t44, R.drawable.t45, R.drawable.t46, R.drawable.t47, R.drawable.t48, R.drawable.t49, R.drawable.t50, R.drawable.t51, R.drawable.t52, R.drawable.t53, R.drawable.t54, R.drawable.t55, R.drawable.t56, R.drawable.t57, R.drawable.t58, R.drawable.t59, R.drawable.t60, R.drawable.t61, R.drawable.t62, R.drawable.t63, R.drawable.t64, R.drawable.t65, R.drawable.t66, R.drawable.t67, R.drawable.t68, R.drawable.t69, R.drawable.t70, R.drawable.t71, R.drawable.t72, R.drawable.t73, R.drawable.t74, R.drawable.t75, R.drawable.t76, R.drawable.t77, R.drawable.t78, R.drawable.t79, R.drawable.t80, R.drawable.t81, R.drawable.t82, R.drawable.t83, R.drawable.t84, R.drawable.t85, R.drawable.t86, R.drawable.t87, R.drawable.t88, R.drawable.t89, R.drawable.t90};
    private int number;
    private boolean tick;
    public Ticket(int number,boolean tick){
        this.number=number;
        this.tick=tick;
    }

    public int getNumber() {
        return number;
    }

    public void setTick(boolean tick) {
        this.tick = tick;
    }

    public boolean isTick() {
        return tick;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getImage2() {
        return drawableImage2[number];
    }

    public int getImage() {
        return drawableImage[number];
    }
}
