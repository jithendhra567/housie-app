package com.jithendhra.housie.Ticket;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.jithendhra.housie.R;


public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.ViewHolder>{
    private Ticket[] tickets;
    private Context context;
    public TicketAdapter(Context context,Ticket[] tickets) {
        this.tickets = tickets;
        this.context = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.ticket_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.imageView.setImageResource(tickets[position].getImage());
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels*0.099);
        ViewGroup.LayoutParams layoutParams = holder.imageView.getLayoutParams();
        layoutParams.width=width;
        layoutParams.height=width;
        holder.imageView.setLayoutParams(layoutParams);
        final Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        holder.tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) vibrator.vibrate(VibrationEffect.createOneShot(30, VibrationEffect.DEFAULT_AMPLITUDE));
                else vibrator.vibrate(30);
                if(!tickets[position].isTick()) {
                    setLocked(holder.imageView);
                    holder.imageView.setImageResource(tickets[position].getImage2());
                    tickets[position].setTick(true);
                }
                else {
                    setUnlocked(holder.imageView);
                    holder.imageView.setImageResource(tickets[position].getImage());
                    tickets[position].setTick(false);
                }
            }
        });
    }
    public static void  setLocked(ImageView v) {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
        v.setColorFilter(cf);
        v.setImageAlpha(128);
    }
    public static void  setUnlocked(ImageView v) {
        v.setColorFilter(null);
        v.setImageAlpha(255);
    }
    @Override
    public int getItemCount() {
        return tickets.length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        LinearLayout tick;
        ViewHolder(View itemView) {
            super(itemView);
            this.imageView =  itemView.findViewById(R.id.number);
            this.tick = itemView.findViewById(R.id.num);
        }
    }
}
