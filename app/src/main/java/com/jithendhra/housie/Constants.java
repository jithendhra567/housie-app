package com.jithendhra.housie;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.firestore.DocumentSnapshot;
import com.jithendhra.housie.User.UserInfo;

import java.util.List;
import java.util.Map;

public class Constants {
    public static String PHONE_NUMBER="phonenumber",RANDOMNUMBERLIST="randomnumberslist",RANDOMNUMBER="randomnumbers",START="start";
    public static String USER_NAME="username",USER_NAMES="usernames",USERS="users",BOTS="bots",CUSTOMROOM="customroom",NUMBEROFTICKETS="numberoftickets",TYPEOFCOINS="typeofcoins";
    public static String ROOM_ID="roomid",PLAYER="player",LEADER="leader",ONLINE="online";
    public static String SUPER_COINS="supercoins",COINS="coins",NUMBER_OF_MATCHES="numberofmatches",
            WIN_COINS="wincoins",WIN_SUPER_COINS="winsupercoins",LUCK_RATIO="luckratio",CONSUMED_COINS="consumedcoins",CONSUMED_SUPER_COINS="consumedsupercoins",AVATAR="avatar";
    public static UserInfo userInfo;
    public static String DATA="data",ROOMIDS="roomids",CHATROOM="chatroom",CHATMESSAGE="chatmessage",CLAIMREWARD="claimrewards";
    public static String FIRSTFIVE="firstfive",TOPLINE="topline",MIDDLELINE="middleline",BOTTOMLINE="bottomline",HOUSIE="housie",NOWINNER="no winners";
    public static int timer=50;
    public static String MATCHING_0="matching_coins",Matching_1="matching_supercoins",MATCH_0="MATCHING_0",MATCH_1="MATCHING_1",USER_0="users_0",USER_1="users_1";
    public static String ONLINE_CHECKS="onlinechecks",MESSAGE="message",AREYOUONLINE="are you online ?",YES="yes";
    public static String REFERRAL="referral",USEDTIMES="user times";
    public static UserInfo getUserInfo() {
        return userInfo;
    }
    public static void setUserInfo(UserInfo userInfo) {
        Constants.userInfo = userInfo;
    }
    public static String MAINTAINENCE ="maintainence",UPDATES="updates",VERSION="version";
    public static Map<String,String> winners,prizepool;
    public static Map<String, String> getPrizepool() {
        return prizepool;
    }
    public static void setPrizepool(Map<String, String> prizepool) {
        Constants.prizepool = prizepool;
    }
    public static boolean isBotCreater = false;
    public static String TOURNAMENTS="tournaments",ACTIONTEXT="actiontext",ANNOUNCEMENTS="announcements",TYPE="type",ACTION="action",DESCRIPTION="description",TITLE="title",POSTER="poster",TUTORIAL="tutorial";
    public static List<DocumentSnapshot> getUpdates() {
        return updates;
    }

    public static void setUpdates(List<DocumentSnapshot> updates) {
        Constants.updates = updates;
    }

    public static List<DocumentSnapshot> updates;
    public static Map<String, String> getItems() {
        return items;
    }
    public static void setItems(Map<String, String> items) {
        Constants.items = items;
    }
    public static Map<String,String> items;
    public static Map<String, String> getWinners() {
        return winners;
    }
    public static void setWinners(Map<String, String> winners) {
        Constants.winners = winners;
    }
    public static String REDEEM = "REDEEM",TRANSCOMPLETED="transactioncompleted",TRANSPENDING="transactionpending",UPIID="upiid",TRANSID="transactionid",AMOUNT="amount";
}
